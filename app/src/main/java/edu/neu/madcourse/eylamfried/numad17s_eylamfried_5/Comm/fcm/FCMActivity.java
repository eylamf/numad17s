package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Comm.fcm;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 3/5/17.
 */

public class FCMActivity extends AppCompatActivity {
    /*
    private static final String TAG = FCMActivity.class.getSimpleName();
    private String userKey="";
    //private CoordinatorLayout mcoor;

    private static final String SERVER_KEY = "key=AAAAR3tkV2s:APA91bHaprO6R1SeVKslXGgCNosAgdB0nsatzkOVFiuEKUJiuah54qtP82y1Qa_2vGUQ_gd1VX_gw_GBhwK3saPKqErVjysv1Z4VXFJAaXsFxiEtpnZWOcyU-J7ulj5Tyc6zksK5bKiY";

    private static final String CLIENT_REGISTRATION_TOKEN = "evIxZL3LoHg:APA91bGC68mFpAiTg8lD68j5cmLv1Hei8rhFA7EtX36WgGMSVZ8KGOcU7CxDZ12GP3OA9lA_tXC_7gWGR2oEQ67DkN1GHcUa1Zk9X8hXvt_Tbs6uo1doznPoVWS-kx9ftkUIUGuxR3en";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fcm);

        Button logTokenButton = (Button) findViewById(R.id.logTokenButton);

    }
    */
    private static final String TAG = FCMActivity.class.getSimpleName();
    private static final String SERVER_KEY = "key=AAAAR3tkV2s:APA91bHaprO6R1SeVKslXGgCNosAgdB0nsatzkOVFiuEKUJiuah54qtP82y1Qa_2vGUQ_gd1VX_gw_GBhwK3saPKqErVjysv1Z4VXFJAaXsFxiEtpnZWOcyU-J7ulj5Tyc6zksK5bKiY";
    private static final String CLIENT_REGISTRATION_TOKEN = "evIxZL3LoHg:APA91bGC68mFpAiTg8lD68j5cmLv1Hei8rhFA7EtX36WgGMSVZ8KGOcU7CxDZ12GP3OA9lA_tXC_7gWGR2oEQ67DkN1GHcUa1Zk9X8hXvt_Tbs6uo1doznPoVWS-kx9ftkUIUGuxR3en";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fcm);

        Button logTokenButton = (Button)findViewById(R.id.logTokenButton);
        /*logTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get token
                /String token = FirebaseInstanceId.getInstance().getToken();

                // log and toast
                String msg = getString(R.string.msg_token_fmt, token);
                Log.d(TAG, msg);
                Toast.makeText(FCMActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    public void pushDNotification() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                pushNotification();
            }
        }).start();
    }

    private void pushNotification() {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        try {
            jNotification.put("title", "Google I/O 2016");
            jNotification.put("body", "Firebase Cloud Messaging (App)");
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "OPEN_ACTIVITY_1");

            // If sending to a single client
            jPayload.put("to", CLIENT_REGISTRATION_TOKEN);

            /*
            // If sending to multiple clients (must be more than 1 and less than 1000)
            JSONArray ja = new JSONArray();
            ja.put(CLIENT_REGISTRATION_TOKEN);
            // Add Other client tokens
            ja.put(FirebaseInstanceId.getInstance().getToken());
            jPayload.put("registration_ids", ja);
            */

            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);


            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "run: " + resp);
                    Toast.makeText(FCMActivity.this,resp,Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

    }
    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }
}



