package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

/**
 * Created by eylamfried on 4/22/17.
 */

public class ChatDetails {
    private String name;

    private String num;

    public ChatDetails(String name, String num) {
        this.name = name;

        this.num = num;
    }

    public String getName() {
        return this.name;
    }



    public String getNum() {
        return this.num;
    }
}
