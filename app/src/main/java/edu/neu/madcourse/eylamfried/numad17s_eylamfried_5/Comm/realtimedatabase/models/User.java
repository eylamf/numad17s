package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Comm.realtimedatabase.models;

/**
 * Created by eylamfried on 3/14/17.
 */

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {

    public String username;
    public String score;

    public User() {

    }

    public User(String username, String score) {
        this.username = username;
        this.score = score;
    }
}
