package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;


/**
 * Created by eylamfried on 2/16/17.
 */

public class CharTile {

    //march 23 - changed - took "final" off
    //private ScroggleFragment fGame;

    private ScroggleFrag mGame;
    private View view;
    private CharTile mSubTiles[];
    public String letter;
    private boolean isSelected;
    private boolean isEmpty;

    public CharTile(ScroggleFrag game) {
        this.mGame = game;
        this.isSelected = false;
        this.isEmpty = false;
    }


    //march 23


    public boolean getIsEmpty() {
        return this.isEmpty;
    }

    public void setIsEmpty(boolean bool) {
        this.isEmpty = bool;
    }

    public void setView(View newView) {
        this.view = newView;
    }

    public View getView() {
        return this.view;
    }

    public CharTile[] getSubTiles() {
        return this.mSubTiles;
    }

    public String getLetter() { return this.letter; }

    public void setLetter(String newLetter) { this.letter = newLetter; }

    public void setSubTiles(CharTile[] tiles) { this.mSubTiles = tiles; }

    public void setSelected(boolean bool) {
        this.isSelected = bool;
    }

    public boolean getIsSelected() {
        return this.isSelected;
    }

    public void animate() {
        Animator anim = AnimatorInflater.loadAnimator(mGame.getActivity(),
                R.animator.tictactoe);
        if (getView() != null) {
            anim.setTarget(getView());
            anim.start();
        }
    }
    /*
    public void timer(View v) {
        Animation flash = AnimationUtils.loadAnimation(mGame.getActivity().getApplicationContext(),
                R.anim.flash);
        v.startAnimation(flash);
    }
    */

    public void updateDrawableState() {
        if (view == null) {
            return;
        }
        int level = getLevel();
        boolean flag = getIsSelected();
        if (view.getBackground() != null) {
            if (flag) {
                view.getBackground().setLevel(level);
            } else {
                view.getBackground().setLevel(level);
            }
        }
        if (view instanceof Button) {
            view.getBackground().setLevel(level);
        }
    }

    private int getLevel() {
        int level;
        if (getIsSelected()) {
            level = R.drawable.letter_green;
        } else {
            level = R.drawable.letter_available;
        }
        if (view instanceof Button) {
            level = R.drawable.letter_gray;
        }
        return level;
    }

}
