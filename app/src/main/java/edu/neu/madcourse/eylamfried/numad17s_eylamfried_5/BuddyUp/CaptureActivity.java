package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.model.Cap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

import static java.lang.Integer.parseInt;

/**
 * Created by eylamfried on 4/14/17.
 */

public class CaptureActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    // init firebase
    private FirebaseStorage mStorage;
    private StorageReference user_ref;
    private FirebaseAuth auth;
    private FirebaseDatabase mDatabase;
    private DatabaseReference ref_users;

    // init widgets, camera, and surface
    private Camera camera;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private boolean camCondition = false;
    private Button captureButton;
    private Button checkButton;
    private Button xButton;

    private Button galleryBtn;

    // init bitmap of image
    private byte[] image_data;

    // list of image datas
    List<byte[]> image_datas = new ArrayList<>();

    // hashmap of image bytes
    Map<Integer, byte[]> images_map = new HashMap<>();

    // the title for buddy up home / the recipient name
    private String recipient_name;

    // counter for images in firebase
    private final int[] bytes_counter = new int[1];

    // prog dialog for uploading image

    private int currentCount;

    private boolean checker = false;

    // GROUP FEATURE

    private boolean isGroup = false;
    private String group_name = "";
    private String group_mems = "";
    private List<String> group_members = new ArrayList<>();

    private int groupCount;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bu_camera_screen_2);
        setTitle("Capture");

        // firebase
        mStorage = FirebaseStorage.getInstance();
        auth = FirebaseAuth.getInstance();
        user_ref = mStorage.getReference().child("BU Users").child(auth.getCurrentUser().getDisplayName());
        mDatabase = FirebaseDatabase.getInstance();
        ref_users = mDatabase.getReference().child("BU Users");


        // init gallery button
        galleryBtn = (Button) findViewById(R.id.gallery_button);

        // init x and check buttons
        xButton = (Button) findViewById(R.id.bu_cam_x);
        checkButton = (Button) findViewById(R.id.bu_cam_check);

        // set the format of the window to UNKNOWN
        getWindow().setFormat(PixelFormat.UNKNOWN);
        // referring the id of the surface view
        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        // getting access to the surface of surfaceView and return it to the surfaceHolder
        surfaceHolder = surfaceView.getHolder();
        // add call back to this context
        surfaceHolder.addCallback(this);
        // set the surface type
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);

        // init capture button
        captureButton = (Button) findViewById(R.id.camera_btn);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                camera.takePicture(null, null, null, callback);
                checkButton.setEnabled(true);
            }
        });

        try {
            // assign the value for the recipient name
            recipient_name = this.getIntent().getExtras().get("recipient name").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            group_name = this.getIntent().getExtras().get("Group name").toString();
            System.out.println("This is the group_name " + group_name);
            ref_users.child(auth.getCurrentUser().getUid()).child("Groups").child("G: " + group_name).child("Members").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                        System.out.println("This is the members map 3: " + map);
                        group_members.clear();
                        group_members.addAll(map.keySet());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            System.out.println("This is the group members : " + group_members);
            isGroup = true;
        } catch (Exception e) {
            e.printStackTrace();
        }


        //

        // in general, dont upload phot unless check button is clicked

        // image counter
        int counter = 0;

        // listener for check button on camera - uploads the image to firebase storage
        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*String bytes = Base64.encodeToString(image_data, Base64.NO_WRAP);
                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(recipient_name).child("Bytes counter").setValue(bytes_counter[0]);
                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(recipient_name).child("Bytes " + bytes_counter[0]).setValue(bytes);
                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(recipient_name).child("Bytes counter").setValue(bytes_counter[0] + 1);*/


                // set up the shared Pref
                /*SharedPreferences sharedPref = getSharedPreferences("Images", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("Photo1", bytes);
                editor.apply();*/

                //

                String filename = auth.getCurrentUser().getDisplayName() + "_" + new Date().getDay() + "_" + new Date().getMinutes() + "_" +  new Date().getSeconds();
                final View v = view;
                try {
                    user_ref.child(filename).putBytes(image_data).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            if (task.isSuccessful()) {

                                if (images_map.get(1) == null) {
                                    images_map.put(1, image_data);
                                } else if (images_map.get(2) == null) {
                                    images_map.put(2, image_data);
                                } else if (images_map.get(3) == null) {
                                    images_map.put(3, image_data);
                                }

                                // add to hashmap of images
                                //images_map.put(image_datas.size(), image_data);
                                image_datas.add(image_data);

                                Snackbar.make(v, "Photo " + images_map.size() + " added", Snackbar.LENGTH_SHORT).setAction("Action", null).show();


                                camera.startPreview();
                                checkButton.setEnabled(false);

                                if (images_map.values().size() == 3) {
                                    captureButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_camera_check_x_buttons));
                                    captureButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            CaptureActivity.this.onBackPressed();
                                        }
                                    });
                                }

                                if (image_datas.size() == 2) {
                                    CaptureActivity.this.onBackPressed();
                                    CaptureActivity.this.onBackPressed();
                                }

                            }
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }




            }
        });
        // click listener for "x" button on camera - discards the current photo
        xButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    camera.startPreview();
                    checkButton.setEnabled(false);

                }catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        // gallery section
        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog gallery = new AlertDialog.Builder(CaptureActivity.this).create();
                View galleryView = getLayoutInflater().inflate(R.layout.bu_gallery_alert, null);
                final ImageView img1 = (ImageView) galleryView.findViewById(R.id.image1);
                final ImageView img2 = (ImageView) galleryView.findViewById(R.id.image2);
                final ImageView img3 = (ImageView) galleryView.findViewById(R.id.image3);
                final Button image_x = (Button) galleryView.findViewById(R.id.image_x);
                final Button image_x2 = (Button) galleryView.findViewById(R.id.image_x_2);
                final Button image_x3 = (Button) galleryView.findViewById(R.id.image_x_3);

                image_x.setVisibility(View.INVISIBLE);
                image_x2.setVisibility(View.INVISIBLE);
                image_x3.setVisibility(View.INVISIBLE);

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;


                try {

                    Bitmap bm = BitmapFactory.decodeByteArray(images_map.get(1), 0, images_map.get(1).length, options);
                    img1.setImageBitmap(bm);
                    //img1.setRotation(90);
                    image_x.setVisibility(View.VISIBLE);
                    // img1.setRotation(90);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    Bitmap bm2 = BitmapFactory.decodeByteArray(images_map.get(2), 0, images_map.get(2).length, options);
                    img2.setImageBitmap(bm2);
                    image_x2.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    Bitmap bm3 = BitmapFactory.decodeByteArray(images_map.get(3), 0, images_map.get(3).length, options);
                    img3.setImageBitmap(bm3);
                    image_x3.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                image_x.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        img1.setImageBitmap(null);
                        images_map.remove(1);
                        image_x.setVisibility(View.INVISIBLE);
                        captureButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_camera_button));
                        captureButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                camera.takePicture(null, null, null, callback);
                                checkButton.setEnabled(true);
                            }
                        });
                    }
                });

                image_x2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        img2.setImageBitmap(null);
                        images_map.remove(2);
                        image_x2.setVisibility(View.INVISIBLE);
                        captureButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_camera_button));
                        captureButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                camera.takePicture(null, null, null, callback);
                                checkButton.setEnabled(true);
                            }
                        });
                    }
                });

                image_x3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        img3.setImageBitmap(null);
                        images_map.remove(3);
                        image_x3.setVisibility(View.INVISIBLE);
                        captureButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_camera_button));
                        captureButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                camera.takePicture(null, null, null, callback);
                                checkButton.setEnabled(true);
                            }
                        });
                    }
                });

                gallery.setView(galleryView);
                gallery.show();
            }
        });
    }

    Camera.PictureCallback callback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera c) {
            image_data = data;

        }
    };


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        camera = Camera.open();
        // make sure the camera is in portrait mode only
        camera.setDisplayOrientation(90);

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

        if (camCondition) {
            // stop preview
            camera.stopPreview();
            // reset value of camCondition
            camCondition = false;
        }

        if (camera != null) {
            try {
                Camera.Parameters parameters = camera.getParameters();
                parameters.setColorEffect(Camera.Parameters.EFFECT_NONE);
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                // set the params of the camera
                camera.setParameters(parameters);

                // set the preview of the camera
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();

                camCondition = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Just did this");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        camera.stopPreview();
        camera.release();
        camera = null;
        camCondition = false;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        System.out.println("This is isGroup : " + isGroup);
        // if it is NOT a group
        if (isGroup == false) {
            ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(recipient_name).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                    // should increment here
                    currentCount = parseInt(map.get("Count")) + 1;
                    checker = true;

                    // testing
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            if (checker) {
                try {
                    System.out.println("Try got called " + (currentCount + 1));
                    Intent intent = new Intent(CaptureActivity.this, BuddyUpHome.class);
                    intent.putExtra("Chat count", Integer.toString(currentCount));
                    intent.putExtra("Recipient name", recipient_name);
                    intent.putExtra("Photo", images_map.get(1));
                    intent.putExtra("Photo2", images_map.get(2));
                    intent.putExtra("Photo3", images_map.get(3));
                    finish();
                    startActivityForResult(intent, 7);
                } catch(IndexOutOfBoundsException e) {
                    Toast.makeText(CaptureActivity.this, "Take 3 photos", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        } else {

            ref_users.child(auth.getCurrentUser().getUid()).child("Groups").child("G: " + group_name).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String count = (String) dataSnapshot.child("Count").getValue();
                    groupCount = parseInt(count) + 1;
                    checker = true;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            if (checker) {
                try {
                    Intent intent = new Intent(CaptureActivity.this, BuddyUpHome.class);
                    intent.putExtra("Is group", isGroup);
                    intent.putExtra("Group count", Integer.toString(groupCount));
                    intent.putExtra("Group members", group_members.toString());
                    intent.putExtra("Group name", group_name);
                    intent.putExtra("Photo", images_map.get(1));
                    intent.putExtra("Photo2", images_map.get(2));
                    intent.putExtra("Photo3", images_map.get(3));
                    finish();
                    startActivityForResult(intent, 7);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("GROUP ERROR");
                }
            }


        }








    }
}
