package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle;

import android.app.Fragment;

/**
 * Created by eylamfried on 2/14/17.
 */

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

public class ScroggleMainFrag extends Fragment {

    private AlertDialog mDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView =
                inflater.inflate(R.layout.scroggle_mainfrag, container, false);

        // Handle buttons here...
        View newButton = rootView.findViewById(R.id.new_button);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ScroggleGameAct.class);
                getActivity().startActivity(intent);
            }
        });

        View ackButton = rootView.findViewById(R.id.scroggle_ack_button);
        ackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ScroggleAcknowledgements.class);
                startActivity(intent);
            }
        });

        View howToPlayButton = rootView.findViewById(R.id.scroggle_how_to);
        howToPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ScroggleHowTo.class);
                startActivity(intent);
            }
        });

        return rootView;
    }

}
