package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 4/18/17.
 */

public class ChatActivity extends AppCompatActivity {

    // init firebase
    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private DatabaseReference ref_users;

    // init the 3 image views
    private ImageView mImageView1;
    private ImageView mImageView2;
    private ImageView mImageView3;

    // list of imageviews
    private List<ImageView> images = new ArrayList<>();

    // init the button for capture
    private Button capture_button;

    // counter
    private final int[] counter = new int[1];

    private byte[] image_bytes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buddyup_home);

        // set the title : To ...
        setTitle("To " + this.getIntent().getExtras().get("Recipient name"));

        // set up firebase
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        ref_users = database.getReference().child("BU Users");


        // set the capture button
        capture_button = (Button) findViewById(R.id.camera_button);

        capture_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatActivity.this, CaptureActivity.class));
            }
        });
        // set the ImageViews
        mImageView1 = (ImageView) findViewById(R.id.imageView);
        mImageView2 = (ImageView) findViewById(R.id.imageView1);
        mImageView3 = (ImageView) findViewById(R.id.imageView2);
        // add values to the list of images
        images.add(mImageView1);
        images.add(mImageView2);
        images.add(mImageView3);

        String recipient = this.getIntent().getExtras().get("Recipient name").toString();
        ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(recipient).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, String> map = (HashMap<String, String>)dataSnapshot.getValue();
                byte[] data = Base64.decode(map.get("Bytes " + counter), Base64.NO_WRAP);
                image_bytes = data;
                System.out.println("Got here");
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inScaled = false;
                Bitmap bm = BitmapFactory.decodeByteArray(image_bytes, 0, image_bytes.length, options);
                mImageView1.setImageBitmap(bm);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





        /*
        // init the shared Pref receiver
        SharedPreferences sharedPref = getSharedPreferences("Images", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        String bytes_string = sharedPref.getString("Photo1", "");

        // convert bytes_string back to a byte[]
        byte[] data = Base64.decode(bytes_string, Base64.NO_WRAP);

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length, options);
            // if the imageview if empty
            if (mImageView1.getDrawable() != null) {
                if (mImageView2.getDrawable() != null) {
                    if (mImageView3.getDrawable() != null) {
                        System.out.println("FILLED");
                    } else {
                        mImageView3.setImageBitmap(bm);
                    }
                } else {
                    mImageView2.setImageBitmap(bm);
                }
            } else {
                mImageView1.setImageBitmap(bm);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        editor.remove("Photo1");
        */
    }

}
