package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Communication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;


/**
 * Created by eylamfried on 3/16/17.
 */

public class CommAcknowledgement extends AppCompatActivity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivty_comm_ack);

        textView = (TextView) findViewById(R.id.comm_ack_text);

        textView.setText(R.string.comm_ack_string);


    }


}
