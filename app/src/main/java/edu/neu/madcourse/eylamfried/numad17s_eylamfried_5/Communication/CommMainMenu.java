package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Communication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Comm2.MainAct;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 3/26/17.
 */

public class CommMainMenu extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comm_menu);

        firebaseAuth = FirebaseAuth.getInstance();
    }

    public void startCommChat(View v) {

        Intent intent = new Intent(this, MainAct.class);
        startActivity(intent);
    }

    public void startCommAddUser(View v) {
        Intent intent = new Intent(this, RegisterUserActivity.class);
        startActivity(intent);
    }

    public void startCommAck(View v) {
        Intent intent = new Intent(this, CommAcknowledgement.class);
        startActivity(intent);
    }

    public void signoutUser(View v) {
        firebaseAuth.signOut();
        Toast.makeText(CommMainMenu.this, "Logged out", Toast.LENGTH_SHORT).show();
        finish();

    }
}
