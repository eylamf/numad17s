package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Communication;

/**
 * Created by eylamfried on 3/15/17.
 */

public class ChatMessage {
    String name;
    String message;

    public ChatMessage() {}

    public ChatMessage(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }
}