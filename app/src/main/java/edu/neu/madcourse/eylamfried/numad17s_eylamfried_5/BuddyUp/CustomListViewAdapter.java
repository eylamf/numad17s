package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 4/18/17.
 */

public class CustomListViewAdapter extends ArrayAdapter<String> {

    private FirebaseAuth auth;
    private FirebaseDatabase mDatabase;
    private DatabaseReference ref_users;
    private DatabaseReference ref_users_chats;

    private String score_temp = "";

    public CustomListViewAdapter(Context context, List<String> resources) {

        super(context, R.layout.bu_custom_friend_list_item, resources);
        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        ref_users = mDatabase.getReference().child("BU Users");


    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View customView = layoutInflater.inflate(R.layout.bu_custom_friend_list_item, parent, false);

        // get a ref for everything
        final String singleItem = getItem(pos);
        final TextView username = (TextView) customView.findViewById(R.id.home_list_username);
        // TODO add user score
        final TextView userScore = (TextView) customView.findViewById(R.id.home_list_score);

        username.setText(singleItem);

        if (!singleItem.contains("G:")) {
            ref_users.child(auth.getCurrentUser().getUid()).child("Chats").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Map<String, String> map = (HashMap<String, String>) dataSnapshot.child(username.getText().toString()).getValue();
                    userScore.setText(map.get("Score"));
                    System.out.println("This is the score map : " + map);
                    System.out.println("This is the name of the user : " + username.getText().toString());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            ref_users.child(auth.getCurrentUser().getUid()).child("Chats").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String score = (String) dataSnapshot.child(singleItem.substring(3, singleItem.length())).child("Score").getValue();
                    userScore.setText(score);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }





        return customView;

    }

}
