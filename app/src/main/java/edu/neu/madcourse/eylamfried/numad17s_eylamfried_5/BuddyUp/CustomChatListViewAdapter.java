package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;
import java.util.List;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 4/19/17.
 */

public class CustomChatListViewAdapter extends ArrayAdapter<String> {

    private String date_string = "";

    // firebase
    private FirebaseAuth auth;
    private FirebaseDatabase mDatabase;
    private DatabaseReference ref_users;
    private String chat_date = "";

    // get only the name without the #
    private String[] name_array = new String[2];


    public CustomChatListViewAdapter(Context context, List<String> resource) {
        super(context, R.layout.bu_chats_alert_row, resource);
        // init firebase
        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        ref_users = mDatabase.getReference().child("BU Users");
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View customView = layoutInflater.inflate(R.layout.bu_chats_alert_row, parent, false);

        // init widgets
        final String singleItem = getItem(pos);

        TextView username = (TextView) customView.findViewById(R.id.chat_list_username);
        final TextView date = (TextView) customView.findViewById(R.id.chat_date);
        date_string = date.getText().toString();
        username.setText(singleItem);

        // init the name array to the username after using "split" method
        // EX: "alice 2" -> ["alice", "2"]
        name_array = username.getText().toString().split(" ");

        /*if (chat_date.equalsIgnoreCase("")) {
            // get the time/date
            ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(name_array[0]).child(username.getText().toString()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String data = (String) dataSnapshot.child("Time").getValue();
                    System.out.println("This is chat_date: "  + data);
                    chat_date = data;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } else {
            // init the time/date on the list view
            date.setText(chat_date);
            System.out.println("This is date: " + date.getText().toString());
        }*/

        // get the time/date
        ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(name_array[0]).child(username.getText().toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String data = (String) dataSnapshot.child("Time").getValue();
                System.out.println("This is chat_date: "  + data);
                chat_date = data;
                date.setText(data);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





        return customView;
    }


}
