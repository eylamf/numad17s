package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;


/**
 * Created by eylamfried on 2/15/17.
 */

public class ScroggleControlFrag extends Fragment {

    private ScroggleFrag scroggleFrag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.scroggle_control_fragment, container, false);

        final View submit = root.findViewById(R.id.submit_button);
        final View pause = root.findViewById(R.id.pause_button);
        final View resume = root.findViewById(R.id.button_resume);
        final View quit = root.findViewById(R.id.quit_button);
        final View mute = root.findViewById(R.id.mute_button);

        submit.setVisibility(View.VISIBLE);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ScroggleGameAct) getActivity()).scroggleFrag.submitWord();
            }
        });

        resume.setVisibility(View.INVISIBLE);
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resume.setVisibility(View.VISIBLE);
                pause.setVisibility(View.INVISIBLE);
                ((ScroggleGameAct) getActivity()).scroggleFrag.onPause();
            }
        });
        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pause.setVisibility(View.VISIBLE);
                resume.setVisibility(View.INVISIBLE);
                ((ScroggleGameAct) getActivity()).scroggleFrag.onResume();
            }
        });
        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ScroggleGameAct) getActivity()).scroggleFrag.quit();
            }
        });

        mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ScroggleGameAct) getActivity()).scroggleFrag.muteMusic();
            }
        });
        return root;
    }


}
