package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.AssetManager;

import android.media.AudioManager;
import android.media.MediaPlayer;

import android.media.SoundPool;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;

import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import java.util.concurrent.TimeUnit;


import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Communication.User;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.TwoPlayerScroggle.TwoPlayerHomeMenu;

import static java.lang.Integer.parseInt;


/**
 * Created by eylamfried on 2/16/17.
 */

public class ScroggleFrag extends Fragment {

    private static final String TAG = ScroggleFrag.class.getSimpleName();

    public int score = 0;

    static private int[] largeGridIDs = {R.id.large1, R.id.large2, R.id.large3, R.id.large4,
            R.id.large5, R.id.large6, R.id.large7, R.id.large8, R.id.large9};

    static private int[] smallGridIDs = {R.id.small1_s, R.id.small2_s, R.id.small3_s, R.id.small4_s, R.id.small5_s,
            R.id.small6_s, R.id.small7_s, R.id.small8_s, R.id.small9_s};


    private CharTile mEntireBoard = new CharTile(this);
    private CharTile mLargeTiles[] = new CharTile[9];
    private CharTile mSmallTiles[][] = new CharTile[9][9];
    private int mLastLarge;
    private int mLastSmall;
    private Set<CharTile> mAvailable = new HashSet<CharTile>();
    static private List<List<Integer>> letterPosns = new ArrayList<List<Integer>>();
    static private List<String> listOfWords = new ArrayList<String>();
    public MyTimer countDownTimer;
    public Handler mHandler = new Handler();
    private final long interval = 1000;
    public long timeRemaning=0;
    private TextView text;
    private TextView textScore;
    private int phase = 1;
    //private String[] phaseTwoWords = new String[100];
    //private Map<Integer,String> listOfWordsFormed = new HashMap<Integer,String>();
    private String letterArray = "";
    private Set<CharTile> nextMoves = new HashSet<>();
    private String gameData="";
    private int[] scoreForWordsFormed = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    private char[][] letterState = new char[9][9];
    private boolean[] isValidLargeGrid = {false, false, false, false, false,false,false,false,false};
    private View rView;

    public Button submitBtn;
    private Map<Integer,ArrayList<Integer>> smallIdsWhichFomWord = new HashMap<Integer,ArrayList<Integer>>();
    public int mPhaseOnePoints=0;
    private int mPhaseTwoPoints=0;
    private int pointsForNumberOfWordsFound = 0;
    private int pointsForLetters = 0;
    private String phaseTwoWord;
    private int pointsForNineLetterWords = 0;
    public MediaPlayer mediaPlayer;
    //
    public List<CharTile> blueButtons = new ArrayList<>();
    private List<CharTile> greenTiles = new ArrayList<>();
    private List<CharTile> redTiles = new ArrayList<>();
    public TreeMap<Integer, String> wordsFound = new TreeMap<>();
    private int treeMapIndex = 1;

    public List<String> nineLetterWords = new ArrayList<>();

    private List<String> currentWordBuilder = new ArrayList<>();
    private List<Integer> largeGridsWithWordFound = new ArrayList<>();
    private List<String> phaseTwoLetters = new ArrayList<>();
    public SoundPool mSoundPool;
    public int mSoundTile;
    public int mSoundChangeTile;

    // two player mode changes
    private String amount_players = "1";

    FirebaseDatabase mDatabase;
    DatabaseReference ref;
    DatabaseReference refOrder;
    DatabaseReference refScore;
    FirebaseAuth first_auth;
    private char[][] copyBoard = new char[9][9];
    private List<String> wordsOnBoard = new ArrayList<>();
    private User player;

    //end game AlertDialog
    private AlertDialog mBuilder;
    private Button exitGame;
    private Button playAgainButton;
    private Button challengeFriendButton;

    // TODO 2 player mode convert 2d array of posns to hashMap
    private Map<Integer, String> posn_map = new HashMap<>();
    DatabaseReference refMap;

    DatabaseReference outerRef;



    /*
    public ScroggleFrag() {
        this.amount_players = 1;
    }
    */




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //init Firebase
        mDatabase = FirebaseDatabase.getInstance();
        first_auth = FirebaseAuth.getInstance();
        player = new User(first_auth.getCurrentUser().getDisplayName(),score);
        ref = mDatabase.getReference().child("Users").child(first_auth.getCurrentUser().getUid()).child("Words");
        refOrder = mDatabase.getReference().child("Users").child(first_auth.getCurrentUser().getUid()).child("Letter Order");
        refScore = mDatabase.getReference().child("Users").child(first_auth.getCurrentUser().getUid()).child("Score");
        amount_players = this.getActivity().getIntent().getExtras().getString("num_players");

        outerRef = mDatabase.getReference().child("Users");

        System.out.println("AMOUNT PLAYERS: " + amount_players);

        refMap = mDatabase.getReference().child("Users").child(first_auth.getCurrentUser().getUid()).child("Position Map");

        // Retain this fragment across configuration changes.
        currentWordBuilder.clear();
        setRetainInstance(true);
        initGame();
        placeLettersOnGrid();
        readNineWordFile();
        mSoundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        mSoundTile = mSoundPool.load(getActivity(), R.raw.numad_bubble_1, 1);
        mSoundChangeTile = mSoundPool.load(getActivity(), R.raw.scroggle_change_tile, 1);





    }
    //get the copy of the board
    public char[][] getCopyBoard() {
        return this.copyBoard;
    }

    private void clearAvailable() {
        mAvailable.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =
                inflater.inflate(R.layout.large_board_scroggle, container, false);
        initViews(rootView);
        updateAllTiles();



        if (gameData != "" && gameData != null) {
            putState(gameData);
        } else {
            startGame(rootView);
        }
        if (phase == 2) {
            if (timeRemaning < 1000) {
                countDownTimer = new MyTimer(40000, interval);
            } else {
                countDownTimer = new MyTimer(timeRemaning, interval);
            }
        } else {
            if (timeRemaning > 0 ) {
                countDownTimer = new MyTimer(timeRemaning, interval);

            } else {
                countDownTimer = new MyTimer(40000, interval);
            }
        }
        //textScore = (TextView) rootView.findViewById(R.id.score);
        text = (TextView) rootView.findViewById(R.id.timer);
        countDownTimer.start();

        return rootView;
    }



    private void initViews(View rootView) {
        mEntireBoard.setView(rootView);
        for (int large = 0; large < 9; large++) {
            final View outer = rootView.findViewById(largeGridIDs[large]);
            mLargeTiles[large].setView(outer);

            for (int small = 0; small < 9; small++) {
                final Button inner = (Button) outer.findViewById
                        (smallGridIDs[small]);

                final int fLarge = large;
                final int fSmall = small;
                final CharTile smallTile = mSmallTiles[large][small];
                smallTile.setView(inner);
                setPossibleNextMoves(fLarge, fSmall);

                inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        smallTile.animate();
                        System.out.println("Possible moves: " + getPossibleNextMoves(mLastSmall).toString());
                        if (phase == 1) {
                            if (!largeGridsWithWordFound.contains(fLarge)) {
                                if (currentWordBuilder.isEmpty()) {
                                    mSoundPool.play(mSoundTile, 1f, 1f, 1, 0, 1f);
                                    smallTile.setSelected(true);
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.tile_blue));
                                    //add to list of tiles that have already been clicked and changed color
                                    blueButtons.add(smallTile);
                                    currentWordBuilder.add(inner.getText().toString());
                                    mLastLarge = fLarge;
                                    mLastSmall = fSmall;
                                    isValidLargeGrid[fLarge] = true;


                                    //testing phase 2
                                    phaseTwoLetters.add((String)inner.getText());

                                } else if (isValidLargeGrid[fLarge] == true && (getPossibleNextMoves(mLastSmall).contains(fSmall) || mLastSmall == fSmall) &&
                                        !blueButtons.contains(smallTile)) {
                                    mSoundPool.play(mSoundTile, 1f, 1f, 1, 0, 1f);
                                    smallTile.setSelected(true);
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.tile_blue));
                                    currentWordBuilder.add(inner.getText().toString());
                                    blueButtons.add(smallTile);
                                    isValidLargeGrid[fLarge] = true;
                                    mLastLarge = fLarge;
                                    mLastSmall = fSmall;

                                    //testing phase 2
                                    phaseTwoLetters.add((String)inner.getText());


                                } else if (isValidLargeGrid[fLarge] == true && blueButtons.contains(smallTile)) {
                                    mSoundPool.play(mSoundChangeTile, 1f, 1f, 1, 0, 1f);

                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.tile_gray));
                                    blueButtons.remove(smallTile);
                                    currentWordBuilder.remove((String)inner.getText());


                                    // testing phase 2
                                    phaseTwoLetters.remove((String)inner.getText());
                                }



                                /*else if (isValidLargeGrid[fLarge] == true && blueButtons.contains(smallTile) && (getPossibleNextMoves(mLastSmall).contains(fSmall) || mLastSmall == fSmall)) {
                                    mSoundPool.play(mSoundChangeTile, 1f, 1f, 1, 0, 1f);
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.tile_gray));
                                    blueButtons.remove(smallTile);
                                    currentWordBuilder.remove((String)inner.getText());
                                    isValidLargeGrid[fLarge] = true;


                                    //testing phase 2
                                    phaseTwoLetters.remove((String)inner.getText());

                                }*/
                            }
                        } else {
                            mSoundPool.play(mSoundTile, 1f, 1f, 1, 0, 1f);
                            //allow access to all large grids in phase 2
                            for (int i = 0; i < 9; i++) {
                                isValidLargeGrid[i] = true;
                            }
                            if (blueButtons.contains(smallTile) && !redTiles.contains(smallTile)) {
                                if (currentWordBuilder.isEmpty() && !greenTiles.contains(smallTile)) {
                                    smallTile.setSelected(true);
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.letter_green));
                                    //add to list of tiles that have already been clicked and changed color
                                    greenTiles.add(smallTile);
                                    currentWordBuilder.add((String)inner.getText());
                                    mLastLarge = fLarge;
                                    mLastSmall = fSmall;
                                    isValidLargeGrid[fLarge] = true;


                                    //testing phase 2
                                    phaseTwoLetters.add((String)inner.getText());

                                } else if (isValidLargeGrid[fLarge] == true && (getPossibleNextMoves(mLastSmall).contains(fSmall) || mLastSmall == fSmall) &&
                                        !greenTiles.contains(smallTile)) {
                                    smallTile.setSelected(true);
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.letter_green));
                                    currentWordBuilder.add((String)inner.getText());
                                    greenTiles.add(smallTile);
                                    isValidLargeGrid[fLarge] = true;
                                    mLastLarge = fLarge;
                                    mLastSmall = fSmall;

                                    //testing phase 2
                                    phaseTwoLetters.add((String)inner.getText());


                                } else if (greenTiles.contains(smallTile))/*if (isValidLargeGrid[fLarge] == true && greenTiles.contains(smallTile) && (getPossibleNextMoves(mLastSmall).contains(fSmall) || mLastSmall == fSmall))*/ {
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.tile_red));
                                    redTiles.add(smallTile);
                                    currentWordBuilder.add((String)inner.getText());
                                    isValidLargeGrid[fLarge] = true;
                                }
                            }
                        }
                    }
                });


                /*inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        smallTile.animate();
                        System.out.println("Possible moves: " + getPossibleNextMoves(mLastSmall).toString());
                        if (phase == 1) {
                            if (!largeGridsWithWordFound.contains(fLarge)) {
                                if (currentWordBuilder.isEmpty()) {
                                    mSoundPool.play(mSoundTile, 1f, 1f, 1, 0, 1f);
                                    smallTile.setSelected(true);
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.tile_blue));
                                    //add to list of tiles that have already been clicked and changed color
                                    blueButtons.add(smallTile);
                                    currentWordBuilder.add((String)inner.getText());
                                    mLastLarge = fLarge;
                                    mLastSmall = fSmall;
                                    isValidLargeGrid[fLarge] = true;


                                    //testing phase 2
                                    phaseTwoLetters.add((String)inner.getText());

                                } else if (isValidLargeGrid[fLarge] == true && (getPossibleNextMoves(mLastSmall).contains(fSmall) || mLastSmall == fSmall) &&
                                        !blueButtons.contains(smallTile)) {
                                    mSoundPool.play(mSoundTile, 1f, 1f, 1, 0, 1f);
                                    smallTile.setSelected(true);
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.tile_blue));
                                    currentWordBuilder.add((String)inner.getText());
                                    blueButtons.add(smallTile);
                                    isValidLargeGrid[fLarge] = true;
                                    mLastLarge = fLarge;
                                    mLastSmall = fSmall;

                                    //testing phase 2
                                    phaseTwoLetters.add((String)inner.getText());


                                } else if (isValidLargeGrid[fLarge] == true && blueButtons.contains(smallTile) && (getPossibleNextMoves(mLastSmall).contains(fSmall) || mLastSmall == fSmall)) {
                                    mSoundPool.play(mSoundChangeTile, 1f, 1f, 1, 0, 1f);
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.tile_gray));
                                    blueButtons.remove(smallTile);
                                    currentWordBuilder.remove((String)inner.getText());
                                    isValidLargeGrid[fLarge] = true;


                                    //testing phase 2
                                    phaseTwoLetters.remove((String)inner.getText());

                                }
                            }
                        } else {
                            mSoundPool.play(mSoundTile, 1f, 1f, 1, 0, 1f);
                            //allow access to all large grids in phase 2
                            for (int i = 0; i < 9; i++) {
                                isValidLargeGrid[i] = true;
                            }
                            if (blueButtons.contains(smallTile) && !redTiles.contains(smallTile)) {
                                if (currentWordBuilder.isEmpty()) {
                                    smallTile.setSelected(true);
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.letter_green));
                                    //add to list of tiles that have already been clicked and changed color
                                    greenTiles.add(smallTile);
                                    currentWordBuilder.add((String)inner.getText());
                                    mLastLarge = fLarge;
                                    mLastSmall = fSmall;
                                    isValidLargeGrid[fLarge] = true;


                                    //testing phase 2
                                    phaseTwoLetters.add((String)inner.getText());

                                } else if (isValidLargeGrid[fLarge] == true && (getPossibleNextMoves(mLastSmall).contains(fSmall) || mLastSmall == fSmall) &&
                                        !greenTiles.contains(smallTile)) {
                                    smallTile.setSelected(true);
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.letter_green));
                                    currentWordBuilder.add((String)inner.getText());
                                    greenTiles.add(smallTile);
                                    isValidLargeGrid[fLarge] = true;
                                    mLastLarge = fLarge;
                                    mLastSmall = fSmall;

                                    //testing phase 2
                                    phaseTwoLetters.add((String)inner.getText());


                                } else if (isValidLargeGrid[fLarge] == true && greenTiles.contains(smallTile) && (getPossibleNextMoves(mLastSmall).contains(fSmall) || mLastSmall == fSmall)) {
                                    inner.setBackgroundDrawable(getResources().getDrawable(R.drawable.tile_red));
                                    redTiles.add(smallTile);
                                    currentWordBuilder.add((String)inner.getText());
                                    isValidLargeGrid[fLarge] = true;
                                }
                            }
                        }
                    }
                });*/
            }
        }
    }

    public void setAmountPlayer(String s) {
        this.amount_players = s;
    }

    //TODO changed for 2 players
    private void startGame(View rootView) {

        if (amount_players.equals("1")) {
            Random rand = new Random();
            mEntireBoard.setView(rootView);
            if (phase == 1) {
                for (int large = 0; large < 9; large++) {
                    //init random index to index in list of 9 letter words
                    int randomIndex = rand.nextInt(nineLetterWords.size()) + 1;

                    View outer = rootView.findViewById(largeGridIDs[large]);
                    mLargeTiles[large].setView(outer);
                    List<Integer> pos = letterPosns.get(large);

                    // get a random word
                    String s = nineLetterWords.get(randomIndex);
                    wordsOnBoard.add(s);
                    //player.setWords(wordsOnBoard);
                    posn_map.put(large, letterPosns.get(large).toString());
                    System.out.println(posn_map.get(large));
                    // map of letter order within the user
                    refMap.child(Integer.toString(large)).setValue(posn_map.get(large));
                    System.out.println("LETTER POSNS TEST: " + letterPosns.get(0));
                    // string array of letter order within the user


                    for(int small = 0;small < 9; small++) {
                        int i = pos.get(small);
                        Button inner = (Button) outer.findViewById(smallGridIDs[i]);
                        inner.setText(String.valueOf(s.charAt(small)));
                        final CharTile smallTile = mSmallTiles[large][i];
                        letterState[large][i] = s.charAt(small);
                        smallTile.setLetter(String.valueOf(s.charAt(small)));
                    }
                }
                refOrder.setValue(letterPosns.toString());
                player.setWords(wordsOnBoard);
                ref.setValue(player.getWords().toString());

            }

        } else {


            try {
                // get the words and letter posns off of Firebase using getExtra (from TwoPlayerMainMenu activity)
                String letter_posns = this.getActivity().getIntent().getExtras().getString("letter_posns");
                String user_words = this.getActivity().getIntent().getExtras().getString("words");


                String[] letter_posns_0 = this.getActivity().getIntent().getExtras().getString("sender_letter_map_0").split(", ");
                String[] letter_posns_1 = this.getActivity().getIntent().getExtras().getString("sender_letter_map_1").split(", ");
                String[] letter_posns_2 = this.getActivity().getIntent().getExtras().getString("sender_letter_map_2").split(", ");
                String[] letter_posns_3 = this.getActivity().getIntent().getExtras().getString("sender_letter_map_3").split(", ");
                String[] letter_posns_4 = this.getActivity().getIntent().getExtras().getString("sender_letter_map_4").split(", ");
                String[] letter_posns_5 = this.getActivity().getIntent().getExtras().getString("sender_letter_map_5").split(", ");
                String[] letter_posns_6 = this.getActivity().getIntent().getExtras().getString("sender_letter_map_6").split(", ");
                String[] letter_posns_7 = this.getActivity().getIntent().getExtras().getString("sender_letter_map_7").split(", ");
                String[] letter_posns_8 = this.getActivity().getIntent().getExtras().getString("sender_letter_map_8").split(", ");


                List<String[]> posn_map_list = new ArrayList<>();
                posn_map_list.add(letter_posns_0);
                posn_map_list.add(letter_posns_1);
                posn_map_list.add(letter_posns_2);
                posn_map_list.add(letter_posns_3);
                posn_map_list.add(letter_posns_4);
                posn_map_list.add(letter_posns_5);
                posn_map_list.add(letter_posns_6);
                posn_map_list.add(letter_posns_7);
                posn_map_list.add(letter_posns_8);


                // get rid of [ and ] in each string[] above
                for (int x = 0; x < 9; x++) {
                    posn_map_list.get(x)[0] = posn_map_list.get(x)[0].substring(1).trim();
                    posn_map_list.get(x)[8] = posn_map_list.get(x)[8].substring(0, 1).trim();
                }

                System.out.println("Testing first item of posn_map_list: " + posn_map_list.get(0)[0] +
                posn_map_list.get(8)[8]);
                System.out.println("Testing string[] of letter posn map : " + letter_posns_0[0]);
                // convert the string of words on Firebase to an array
                String[] sender_words = user_words.split(", ");
                // get rid of [ and ] in string -> [ value, value, value] -> value, value, value
                sender_words[0] = sender_words[0].substring(1).trim();
                sender_words[8] = sender_words[8].substring(0, 9).trim();

                // make the actual board with the above information
                for (int lg = 0; lg < 9; lg++) {
                    View outer = rootView.findViewById(largeGridIDs[lg]);
                    mLargeTiles[lg].setView(outer);
                    String[] pos = posn_map_list.get(lg);//letterPosns.get(lg);
                    String s = sender_words[lg];
                    for (int sm = 0; sm < 9; sm++) {
                        int i = parseInt(pos[sm]);
                        Button inner = (Button) outer.findViewById(smallGridIDs[i]);
                        inner.setText(String.valueOf(s.charAt(sm)));
                        final CharTile smallTile = mSmallTiles[lg][i];
                        letterState[lg][i] = s.charAt(sm);
                        smallTile.setLetter(String.valueOf(s.charAt(sm)));
                    }
                }




            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    public void placeLettersOnGrid() {
        List<List<Integer>> positions = new ArrayList<List<Integer>>();
        positions.addAll(Arrays.asList(
                Arrays.asList(0,3,6,7,5,2,1,4,8),
                Arrays.asList(0,1,4,6,3,7,8,5,2),
                Arrays.asList(2,5,7,8,4,6,3,1,0),
                Arrays.asList(2,5,1,0,3,7,6,4,8),
                Arrays.asList(3,0,4,6,7,8,5,1,2),
                Arrays.asList(5,8,4,6,7,3,0,1,2),
                Arrays.asList(8,4,0,3,6,7,5,2,1),
                Arrays.asList(1,5,8,7,6,3,0,4,2),
                Arrays.asList(1,0,3,6,4,2,5,7,8),
                Arrays.asList(6,7,8,5,1,0,3,4,2),
                Arrays.asList(6,3,1,0,4,2,5,8,7),
                Arrays.asList(3,6,4,8,7,5,2,1,0),
                Arrays.asList(5,2,4,0,1,3,6,7,8),
                Arrays.asList(4,1,2,5,8,7,6,3,0),
                Arrays.asList(5,2,1,0,3,4,6,7,8),
                Arrays.asList(8,7,6,3,4,5,2,1,0),
                Arrays.asList(6,4,3,0,1,2,5,8,7),
                Arrays.asList(4,0,1,3,6,7,8,5,2)
        ));

        for(int i=0;i<9;i++) {
            Random random = new Random();
            int index = random.nextInt(positions.size());
            letterPosns.add(positions.get(index));
        }
    }



    private boolean isValidMove(CharTile tile) {
        return nextMoves.contains(tile);
    }

    public void clearMove() {
        nextMoves.clear();
    }

    public void addMove(CharTile tile) {
        nextMoves.add(tile);
    }


    public List<Integer> getPossibleNextMoves(int smallTile) {
        List<Integer> moves = new ArrayList<>();
        switch(smallTile) {
            case 0:
                moves.addAll(Arrays.asList(1,3,4));
                break;
            case 1:
                moves.addAll(Arrays.asList(0,2,3,4,5));
                break;
            case 2:
                moves.addAll(Arrays.asList(1,4,5));
                break;
            case 3:
                moves.addAll(Arrays.asList(0,1,4,6,7));
                break;
            case 4:
                moves.addAll(Arrays.asList(0,1,2,3,5,6,7,8));
                break;
            case 5:
                moves.addAll(Arrays.asList(1,2,4,7,8));
                break;
            case 6:
                moves.addAll(Arrays.asList(3,4,7));
                break;
            case 7:
                moves.addAll(Arrays.asList(3,4,5,6,8));
                break;
            case 8:
                moves.addAll(Arrays.asList(4,5,7));
                break;
        }
        return moves;
    }

    public void initGame() {
        mEntireBoard = new CharTile(this);
        // Create all the tiles
        for (int large = 0; large < 9; large++) {
            mLargeTiles[large] = new CharTile(this);
            for (int small = 0; small < 9; small++) {
                mSmallTiles[large][small] = new CharTile(this);
            }
            mLargeTiles[large].setSubTiles(mSmallTiles[large]);
        }
        mEntireBoard.setSubTiles(mLargeTiles);
        // If the player moves first set which spots are available
        mLastLarge = -1;
        mLastSmall = -1;
        if(phase ==1 ) {
            setAvailableFromLastMove(mLastSmall);
            setPossibleNextMoves(mLastLarge, mLastSmall);
        }
    }
    private void setAvailableFromLastMove(int small) {
        clearAvailable();
        // Make all the tiles at the destination available
        if (small != -1) {
            for (int dest = 0; dest < 9; dest++) {
                CharTile tile = mSmallTiles[small][dest];
                if (!tile.getIsSelected()) {
                    addAvailable(tile);
                }
            }
        }
        // If there were none available, make all squares available
        if (mAvailable.isEmpty()) {
            setAllAvailable();
        }
    }
    private void addAvailable(CharTile tile) {
        tile.animate();
        mAvailable.add(tile);
    }
    private void setAllAvailable() {
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                CharTile tile = mSmallTiles[large][small];
                if(!tile.getIsSelected()) {
                    addAvailable(tile);
                }
            }
        }
    }




    private void updateAllTiles() {
        mEntireBoard.updateDrawableState();
        for (int large = 0; large < 9; large++) {
            mLargeTiles[large].updateDrawableState();
            for (int small = 0; small < 9; small++) {
                mSmallTiles[large][small].updateDrawableState();
            }
        }
    }

    /** Create a string containing the state of the game. */
    public String getState() {
        StringBuilder builder = new StringBuilder();
        builder.append(mLastLarge);
        builder.append(',');
        builder.append(mLastSmall);
        builder.append(',');
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                builder.append(mSmallTiles[large][small]);
                builder.append(',');
            }
        }
        return builder.toString();
    }

    /** Restore the state of the game from the given string. */
    public void putState(String gameData) {
        String[] fields = gameData.split(",");
        int index = 0;
        mLastLarge = parseInt(fields[index++]);
        mLastSmall = parseInt(fields[index++]);
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {

                mSmallTiles[large][small].getLetter();
            }
        }
        setAvailableFromLastMove(mLastSmall);
        updateAllTiles();

    }

    public void readFile(String file) {
        listOfWords.clear();
        BufferedReader reader;
        AssetManager assets = getResources().getAssets();
        InputStream inputStream;

        try {
            inputStream = assets.open(file);
            InputStreamReader inputReader = new InputStreamReader(inputStream);
            reader = new BufferedReader(inputReader, 1024);
            String line;

            while ((line = reader.readLine()) != null) {
                listOfWords.add(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void readNineWordFile() {
        BufferedReader reader = null;
        AssetManager manager = getResources().getAssets();
        InputStream input = null;

        try {
            input = manager.open("NineLetterWords");
            InputStreamReader inputStreamReader = new InputStreamReader(input);
            reader = new BufferedReader(inputStreamReader, 1024);
            String receiveStr = null;
            while ((receiveStr = reader.readLine()) != null) {
                nineLetterWords.add(receiveStr);
                listOfWords.add(receiveStr);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void display(boolean bool) {
        String temp = "";
        for (int i = 0; i < currentWordBuilder.size(); i++) {
            temp += currentWordBuilder.get(i);
        }
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layoutView = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) getView().findViewById(R.id.custom_toast_container));
        TextView text = (TextView) layoutView.findViewById(R.id.text);
        if (bool) {
            text.setText("Word found! : " + temp);
            score+= temp.length();
        } else {
            text.setText("Not valid: " + temp);
            score--;

        }
        Toast toast = new Toast(getActivity().getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layoutView);
        toast.show();
    }

    public void displayPhaseTwoText() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layoutView = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) getView().findViewById(R.id.custom_toast_container));
        TextView text = (TextView) layoutView.findViewById(R.id.text);
        text.setText("PHASE 2: GO!");
        Toast toast = new Toast(getActivity().getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layoutView);
        toast.show();
    }

    public void displayEndGame() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layoutView = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) getView().findViewById(R.id.custom_toast_container));
        TextView text = (TextView) layoutView.findViewById(R.id.text);
        text.setText("GAME OVER \n" + " SCORE: " + Integer.toString(score));
        Toast toast = new Toast(getActivity().getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layoutView);
        toast.show();

    }

    public void displayDuplicateWord() {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layoutView = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) getView().findViewById(R.id.custom_toast_container));
        TextView text = (TextView) layoutView.findViewById(R.id.text);
        text.setText("Word: " + wordArrayToString() + " already exists");
        Toast toast = new Toast(getActivity().getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layoutView);
        toast.show();
    }

/*

    public void initGame() {
        mEntireBoard = new CharTile(this);
        //creat all tiles
        for (int l = 0; l < 9; l++) {
            mLargeTiles[l] = new CharTile(this);
            for (int s = 0; s < 9; s++) {
                mSmallTiles[l][s] = new CharTile(this);
            }
            mLargeTiles[l].setSubTiles(mSmallTiles[l]);
        }
        mEntireBoard.setSubTiles(mLargeTiles);
        //if player moves first set which spots are available
        mLastSmall = -1;
        mLastLarge = -1;
        setAvailableFromLastMove(mLastSmall);
        setValidNextMove(mLastLarge, mLastSmall);
    }
    */



    public void setPossibleNextMoves(int large, int small) {
        clearMove();
        if(large != -1 && small != -1) {

            //get all the possible squares in all directions of the grid with value small
            List<Integer> moves = getPossibleMoves(small);
            for (int i=0; i< moves.size();i++) {
                int gridId = moves.get(i);
                CharTile smallTile = mSmallTiles[large][gridId];
                if (!smallTile.getIsSelected()) {
                    addMove(smallTile);
                }
            }

            for (int l = 0; l < 9; l++) {
                for (int s = 0; s < 9; s++) {
                    if (l != large) {
                        //   if(!mSmallTiles[large][small].getIsChosen()){
                        addMove(mSmallTiles[l][s]);
                        //   }
                    }
                }
            }
        }
        if(nextMoves.isEmpty()){
            setAllNextMoves();
        }
    }

    public void setAllNextMoves() {
        for (int l = 0; l < 9; l++) {
            for (int s = 0; s < 9; s++) {
                addMove(mSmallTiles[l][s]);
            }
        }
    }

    public List<Integer> getPossibleMoves(int small) {
        List<Integer> moves = new ArrayList<>();
        switch(small) {
            case 0: moves.addAll(Arrays.asList(1, 3, 4));
                break;
            case 1: moves.addAll(Arrays.asList(0, 2, 3, 4, 5));
                break;
            case 2: moves.addAll(Arrays.asList(1, 4, 5));
                break;
            case 3: moves.addAll(Arrays.asList(0, 1, 4, 6, 7));
                break;
            case 4: moves.addAll(Arrays.asList(0, 1, 2, 3, 5, 6, 7, 8));
                break;
            case 5: moves.addAll(Arrays.asList(1, 2, 4, 7, 8));
                break;
            case 6: moves.addAll(Arrays.asList(3, 4, 7));
                break;
            case 7: moves.addAll(Arrays.asList(3, 4, 5, 6, 8));
                break;
            case 8: moves.addAll(Arrays.asList(4, 5, 7));
                break;
        }
        return moves;

    }




    public void addScore() {
        this.score += currentWordBuilder.size();

    }


    private String wordArrayToString() {
        String temp = "";
        for (int i = 0; i < currentWordBuilder.size(); i++) {
            temp += currentWordBuilder.get(i);
        }
        return temp;
    }

    public void submitWord() {

        if (wordsFound.containsValue(wordArrayToString())) {
            displayDuplicateWord();
        }

        if (isValidWord(wordArrayToString()) && wordArrayToString().length() > 2 && !wordsFound.containsValue(wordArrayToString())) {
            display(true);
            isValidLargeGrid[mLastLarge] = false;
            largeGridsWithWordFound.add(mLastLarge);
            System.out.println("Words found: " + wordsFound.toString());
            System.out.println("Large tiles w/ words " + largeGridsWithWordFound.toString());
            wordsFound.put(treeMapIndex, wordArrayToString());
            treeMapIndex++;
            currentWordBuilder.clear();
            addScore();
            ((ScroggleGameAct) getActivity()).updateScore();

        } else if (!(wordArrayToString().length() > 2)) {
            try {

            }
            catch(StringIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
        else {
            if (phase == 1) {
                display(false);
                //isValidLargeGrid[mLastLarge] = true;
            } else {
                display(false);
                currentWordBuilder.clear();
            }


            //currentWordBuilder.clear();
        }
    }


    public int getScore() {
        return this.score;
    }

    public boolean isValidWord(String s) {

        if (s.length() == 9) {
            return nineLetterWords.contains(s);
        } else {
            try {
                String temp = s.substring(0, 1).toUpperCase() + s.substring(1, 2).toLowerCase();
                readFile(temp);
                if (listOfWords.contains(s.toLowerCase())) {
                    //Log.d(TAG, letterArray);
                    ToneGenerator foundTone = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                    foundTone.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
                    return true;
                }
            } catch (StringIndexOutOfBoundsException e) {
                e.printStackTrace();
            }

        }
        return false;
    }


    public class MyTimer extends CountDownTimer {

        public MyTimer(long start, long interval) {
            super(start, interval);

        }

        @Override
        public void onTick(long l) {
            String time = String.format("%d : %d",
                    TimeUnit.MILLISECONDS.toMinutes(l),
                    TimeUnit.MILLISECONDS.toSeconds(l) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l)));
            text.setText("Time Remaning: " + time);
            //System.out.println(time.substring(time.length() - 2, time.length()));

            if (time.substring(time.length() - 2, time.length()).equals("12")) {
                Toast.makeText(getActivity(), "Under 10 seconds left!", Toast.LENGTH_SHORT).show();

            }

            /*if (l < 11000 && l > 10500) {
                Toast.makeText(getActivity(), "Only 10 seconds left!", Toast.LENGTH_SHORT).show();
            }*/
        }

        @Override
        public void onFinish() {
            if (phase == 1) {
                displayPhaseTwoText();

                countDownTimer.start();
                phase = 2;
            } else {
                displayEndGame();


                //change player top score if he or she just got better

                outerRef.child(first_auth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                        if (parseInt(map.get("Score")) < score) {
                            refScore.setValue(Integer.toString(score));
                        }

                        //System.out.println(map);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                //refScore.setValue(Integer.toString(score));
                getActivity().finish();

            }



            //((ScroggleGameAct)getActivity()).scroggleFrag.initGame();
            //displayEndGame();

        }
    }

    public void muteMusic() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        } else {
            mediaPlayer.start();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        countDownTimer.cancel();
        ((ScroggleGameAct)getActivity()).scroggleFrag.getView().setVisibility(View.INVISIBLE);
        try {
            mBuilder.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Override
    public void onResume() {
        super.onResume();
        ((ScroggleGameAct)getActivity()).scroggleFrag.getView().setVisibility(View.VISIBLE);
        countDownTimer.start();

    }


    public void quit() {
        countDownTimer.cancel();
        getActivity().finish();
    }









}