package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 2/17/17.
 */

public class ScroggleAcknowledgements extends AppCompatActivity {
    TextView scroggleAck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scroggle_ack);

        scroggleAck = (TextView) findViewById(R.id.scroggle_ack_text);


        String acknowledgement =
                "I used a lot of code from the Ultimate Tic Tac Toe assignment. Primarily, I used code\n" +
                        "regarding how to set up the board, the visuals of the board, tile animations, and in general how\n" +
                        "to set up the structure of the fragments/activities. I also used StackOverflow for debugging and\n" +
                        "changing the board's visuals.";

        scroggleAck.setText(acknowledgement);
    }
}
