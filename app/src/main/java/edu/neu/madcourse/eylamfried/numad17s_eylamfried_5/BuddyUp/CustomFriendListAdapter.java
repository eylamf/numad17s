package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 4/18/17.
 */

public class CustomFriendListAdapter extends ArrayAdapter<String> {
    // add firebase
    private FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private DatabaseReference ref_users = mDatabase.getReference().child("BU Users");
    private DatabaseReference ref_usernames = mDatabase.getReference().child("BU Usernames");

    // map of user ids
    private Map<String, String> user_ids = new HashMap<>();

    public CustomFriendListAdapter(Context context, List<String> resources) {
        super(context, R.layout.bu_friend_notif_alert_row, resources);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View customView = layoutInflater.inflate(R.layout.bu_friend_notif_alert_row, parent, false);

        // get a ref for everything
        final String singleItem = getItem(pos);
        final TextView username = (TextView) customView.findViewById(R.id.username_text);
        Button add = (Button) customView.findViewById(R.id.add_user);
        Button deny = (Button) customView.findViewById(R.id.deny_user);

        final List<String> friends = new ArrayList<>();
        final List<String> uids = new ArrayList<>();

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // map of ids
                ref_usernames.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try{
                            Map<String, String> map = (HashMap<String, String>)dataSnapshot.getValue();
                            System.out.println("This is map : " + map);
                            user_ids.putAll(map);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                /*// add the user to my friend list
                ref_users.child(auth.getCurrentUser().getUid()).child("Friends").child(singleItem).setValue("");
                // add myself to their friends list
                // ref_users.child(user_ids.get(singleItem)).child("Friends").child(auth.getCurrentUser().getDisplayName()).setValue("");
                System.out.println("User ids " + user_ids);
                // change the status of the request
                ref_users.child(auth.getCurrentUser().getUid()).child("Requests").child(singleItem).setValue("Accepted");
                */

                // check if I am already friends with the user - AVOID DUPS
                ref_users.child(auth.getCurrentUser().getUid()).child("Friends").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Map<String, String> map2 = (HashMap<String, String>)dataSnapshot.getValue();
                        System.out.println("this is map2 " + map2);
                        try {

                            friends.clear();
                            friends.addAll(map2.keySet());
                            notifyDataSetChanged();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (!friends.contains(singleItem)) {
                            // add the user to my friend list
                            ref_users.child(auth.getCurrentUser().getUid()).child("Friends").child(singleItem).setValue("");
                            // set the chats child with the new user and total count to 0
                            ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(singleItem).child("Count").setValue("0");

                            // set the score in my firebase
                            ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(username.getText().toString()).child("Score").setValue("0");
                            // set the score in the sender's fb
                            ref_users.child(user_ids.get(singleItem)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child("Score").setValue("0");


                            // add myself to their friends list
                            ref_users.child(user_ids.get(singleItem)).child("Friends").child(auth.getCurrentUser().getDisplayName()).setValue("");
                            // set the chats child with my user name and create the total count child - 0
                            ref_users.child(user_ids.get(singleItem)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child("Count").setValue("0");



                            // change the status of the request
                            ref_users.child(auth.getCurrentUser().getUid()).child("Requests").child(singleItem).setValue("Accepted");
                        }



                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });

        deny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ref_users.child(auth.getCurrentUser().getUid()).child("Requests").child(singleItem).setValue("Denied");
            }
        });


        username.setText(singleItem);


        return customView;

    }
}
