package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp.BuddyUpStart;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp.LoginPage;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Communication.CommMain;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Dictionary.DictionaryApp;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle.ScroggleMainActivity;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.TicTacToe.MainActivity;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.TwoPlayerScroggle.TwoPlayerMain;

public class MainActivity_main extends AppCompatActivity {



    Button tictactoe_go;
    private static final String TAG = MainActivity_main.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_main);


        Log.e(TAG, "in onCreate()");

    }

    //activity to play tic tac toe game
    public void playTicTacToe(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    //start the about page
    public void visitAbout(View view) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    //open the dictionary app
    public void visitDictionary(View view) {
        Intent intent = new Intent(this, DictionaryApp.class);
        startActivity(intent);
        finish();
    }
    //generate an error
    public void generateError(View view) {
        throw new RuntimeException("Successfully generated an error");
    }
    //exit the app
    public void exitApp(View view) {
        finish();
    }

    //play Scroggle
    public void playScroggle(View v) {
        Intent intent = new Intent(this, ScroggleMainActivity.class);
        startActivity(intent);
    }

    public void startComm(View v) {
        Intent intent = new Intent(this, CommMain.class);
        startActivity(intent);
        //setContentView(R.layout.comm_menu);
        //Intent intent = new Intent(this, MainAct.class);
        //startActivity(intent);
    }

    public void startTwoPlayer(View v) {
        Intent intent = new Intent(this, TwoPlayerMain.class);
        startActivity(intent);
    }

//    public void startCommChat(View v) {
//
//        Intent intent = new Intent(this, MainAct.class);
//        startActivity(intent);
//    }
//
//    public void startCommAddUser(View v) {
//        Intent intent = new Intent(this, RegisterUserActivity.class);
//        startActivity(intent);
//    }
//
//    public void startCommAck(View v) {
//        Intent intent = new Intent(this, CommAcknowledgement.class);
//        startActivity(intent);
//    }

    // FINAL PROJECT
    public void startBuddyUp(View v) {
        Intent intent = new Intent(MainActivity_main.this, BuddyUpStart.class);
        startActivityForResult(intent, -1);

        // how to start camera activity
        /*Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivity(intent);*/
    }



}
