package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

import static java.lang.Integer.parseInt;

/**
 * Created by eylamfried on 4/12/17.
 */

public class HomeScreen extends AppCompatActivity {


    // init firebase auth
    private FirebaseAuth auth;

    // init firebase databse
    private FirebaseDatabase mDatabase;

    // init firebase ref for users
    private DatabaseReference ref_users;
    private DatabaseReference ref_usernames;
    private DatabaseReference ref_chats;

    private ListView friends;

    private List<String> friends_names = new ArrayList<>();
    private ArrayAdapter<String> arrayAdapter;

    private static final Integer CAMERA = 0x5;


    private Button add_user_button;
    private TextView users_name;
    private Button groupButton;

    //recipient username for adding a friend
    private String[] recipient;

    private ProgressDialog progressDialog;

    //private Toolbar toolbar;

    // a list of the current user's friends on firebase
    private List<String> firebase_friends = new ArrayList<>();

    // map of usersnames and their IDs
    private Map<String, String> map_ids = new HashMap<>();

    private String sender_name = "";

    private int current_count_for_chat = 0;

    //testing
    private List<String> chat_inbox = new ArrayList<>();

    // groups
    private List<String> group_names = new ArrayList<>();

    // boolean for if there is a new friends req
    private boolean hasFriend = false;
    // TODO - did on plane
    // boolean for if there is a new chat notif
    private boolean hasChat = false;

    private boolean containsChat = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bu_home_activity);

        /*toolbar = (Toolbar) findViewById(R.id.home_toolbar);
        this.setSupportActionBar(toolbar);

        toolbar.setTitle("Home");
        toolbar.setNavigationIcon(R.drawable.bu_back_button);*/

        // init firebase
        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        ref_users = mDatabase.getReference().child("BU Users");
        ref_usernames = mDatabase.getReference().child("BU Usernames");
        ref_chats = mDatabase.getReference().child("BU Chats");
        setTitle("Home");

        try {
            containsChat = (boolean) this.getIntent().getExtras().get("contains chat");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        // set the recipient's name
        recipient = new String[1];
        // welcome back snackbar
        Snackbar.make(this.findViewById(android.R.id.content), "Welcome back, " + auth.getCurrentUser().getDisplayName(), Snackbar.LENGTH_LONG).show();
        // ask for camera permissions
        askForPermission(Manifest.permission.CAMERA, CAMERA);

        // init buttons and texts on view
        add_user_button = (Button) findViewById(R.id.add_user_button);
        users_name = (TextView) findViewById(R.id.user_name);
        groupButton = (Button) findViewById(R.id.bu_group_button);

        // TODO - did on plane
        ref_chats.child(auth.getCurrentUser().getDisplayName()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                    List<String> chats = new ArrayList<String>();
                    chats.clear();
                    chats.addAll(map.keySet());

                    chat_inbox.clear();

                    for (String s : chats) {
                        if (map.get(s).equals("Closed")) {
                            chat_inbox.add(s);
                        }
                    }

                    /*if (chat_inbox.size() > 0) {
                        hasChat = true;
                    }*/


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        // init map of uids
        ref_usernames.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, String> map = (HashMap<String, String>)dataSnapshot.getValue();
                map_ids = map;
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // init boolean of whether there is a friends req waiting
        ref_users.child(auth.getCurrentUser().getUid()).child("Requests").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                    System.out.println("This is the friends req map: " + map);
                    List<String> statuses = new ArrayList<String>();
                    statuses.clear();
                    statuses.addAll(map.values());

                    if (statuses.contains("Pending")) {
                        hasFriend = true;
                        System.out.println("Got here :  hasFriend");
                    }
                    // if this is a new user
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        /*ref_users.child(auth.getCurrentUser().getUid()).child("Chats").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                // must have try catch for if the user is new and this is the first time they are logging in
                try {
                    Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                    System.out.println("This is the chat notif map : " + map);
                    List<String> chats = new ArrayList<String>();
                    chats.clear();
                    // add all the values of the map to the chats list
                    chats.addAll(map.values());
                    chat_inbox.clear();
                    chat_inbox.addAll(chats);
                    // check if any of the chats are "closed"
                    if (chats.contains("Closed")) {
                        hasChat = true;
                        System.out.println("Got here : hasChat event listener");
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        */


        /*// init group list
        ref_users.child(auth.getCurrentUser().getUid()).child("Groups").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, String> map = (Map<String, String>)dataSnapshot.getValue();
                System.out.println("This is the groups map : "+ map);
                group_names.clear();
                group_names.addAll(map.keySet());



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        */
        /*// init friends list
        ref_users.child(auth.getCurrentUser().getUid()).child("Friends").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    Map<String, String> map = (HashMap<String, String>)dataSnapshot.getValue();
                    System.out.println("Friends map: " + map);
                    Set<String> friends_set = map.keySet();
                    //friends_names = new ArrayList<String>();
                    friends_names.clear();
                    friends_names.addAll(friends_set);
                    friends_names.remove(auth.getCurrentUser().getDisplayName());
                    firebase_friends.addAll(group_names);
                    arrayAdapter.notifyDataSetChanged();
                    firebase_friends.addAll(map.keySet());

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        */
        // init friends list with the group list
        ref_users.child(auth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    // map of friends
                    Map<String, HashMap<String, String>> friends_map = (Map<String, HashMap<String, String>>)dataSnapshot.child("Friends").getValue();
                    friends_names.clear();
                    friends_names.addAll(friends_map.keySet());
                }
                // no friends yet
                catch (NullPointerException e) {
                    e.printStackTrace();
                }

                // map of groups
                try {
                    Map<String, String> group_map = (HashMap<String, String>)dataSnapshot.child("Groups").getValue();
                    group_names.clear();
                    group_names.addAll(group_map.keySet());
                    friends_names.addAll(group_names);
                }
                // no groups exist
                catch (NullPointerException e) {
                    e.printStackTrace();
                }

                arrayAdapter.notifyDataSetChanged();



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // add user button onclick
        add_user_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog mBuilder = new AlertDialog.Builder(HomeScreen.this).create();
                View v = getLayoutInflater().inflate(R.layout.bu_add_friend, null);
                final Button request_friend = (Button) v.findViewById(R.id.request_friend_button);
                // set the button to dark
                request_friend.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));
                SearchView search_users = (SearchView) v.findViewById(R.id.search_users);
                search_users.setQueryHint("Search a username");

                // the username of the person to be added
                final String[] recipient = {null};
                mBuilder.setView(v);
                mBuilder.show();

                // send friend request button listener
                request_friend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // set the progress dialog
                        progressDialog = new ProgressDialog(mBuilder.getContext());
                        progressDialog.setMessage("Sending friend request...");



                        if (recipient[0] != null) {
                            progressDialog.show();
                            /*ref_users.child(auth.getCurrentUser().getUid()).child("Friends").child((recipient[0])).setValue("false").addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        if (!firebase_friends.contains(recipient[0])) {
                                            //TODO friends_names.add(recipient[0]);


                                        }
                                        //arrayAdapter.notifyDataSetChanged();
                                        progressDialog.dismiss();

                                    }
                                }
                            });*/


                            /*ref_users.child(auth.getCurrentUser().getUid()).child("Requests").child(recipient[0]).setValue("Pending").addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        progressDialog.dismiss();
                                    }
                                }
                            });*/


                            // add myself to the recipient's firebase friends
                            //ref_users.child(map_ids.get(recipient[0])).child("Friends").child(auth.getCurrentUser().getDisplayName()).setValue("");
                            ref_users.child(map_ids.get(recipient[0])).child("Requests").child(auth.getCurrentUser().getDisplayName()).setValue("Pending").addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    progressDialog.dismiss();
                                }
                            });

                            mBuilder.dismiss();
                            // snackbar to notify the request was sent
                            Snackbar.make(HomeScreen.this.findViewById(android.R.id.content), "Request sent", Snackbar.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mBuilder.getContext(), "No user found", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                // text listener for the searchview
                search_users.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(final String s) {

                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(final String s) {



                        try {
                            ref_usernames.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Map<String, String> map = (HashMap<String, String>)dataSnapshot.getValue();

                                    if (map.get(s) != null) {
                                        // if the user has no friends, a null pointer exception will go off so we catch it here
                                        try {
                                            // check to see if this user name is already a friend and also that it is not myself
                                            if (!friends_names.contains(s) && !(auth.getCurrentUser().getDisplayName().equalsIgnoreCase(s))) {
                                                request_friend.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button));
                                                recipient[0] = s;
                                            }
                                        } catch (NullPointerException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        request_friend.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        return false;
                    }
                });




            }
        });

        // display user's name
        users_name.setText(auth.getCurrentUser().getDisplayName());

        friends = (ListView) findViewById(R.id.friends_list_bu);

        //arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, friends_names);
        arrayAdapter = new CustomListViewAdapter(this, friends_names);
        friends.setAdapter(arrayAdapter);

        ref_users.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

                /*HashMap<String, String> value_map = (HashMap<String, String>) dataSnapshot.getValue();
                System.out.println("testing...." + value_map.get("Username"));
                String value = value_map.get("Username");

                if (friends_names.contains(value) || value.equalsIgnoreCase(auth.getCurrentUser().getDisplayName())) {
                    // do nothing - avoid dups
                } else {
                    friends_names.add(value);
                }

                arrayAdapter.notifyDataSetChanged();
                */

            }

            @Override
            public void onChildChanged(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(com.google.firebase.database.DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // TODO - did on plane - adding group checker (see if the name of the item on the list view contains 'Group:'
        // set friends list item click listener
        friends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int posn, long l) {

                String recipient_name = friends.getItemAtPosition(posn).toString();//friends_names.get(posn);
                System.out.println("This is the rec substring: " + recipient_name.substring(0, 3));

                // get the group name without the "Group:" prefix
                String group_name = recipient_name.substring(3, recipient_name.length());

                // create the intent
                Intent intent = new Intent(HomeScreen.this, CaptureActivity.class);

                // TODO  might want to user "friends.getItemAtPosition(posn)
                try {

                    if (recipient_name.substring(0, 3).equals("G: ")) {
                        // pass both the group name and it's members to the new intent
                        intent.putExtra("Group name", group_name);

                    } else {
                        intent.putExtra("recipient name", recipient_name);
                    }

                } catch (StringIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }




                startActivityForResult(intent, 6);
            }
        });


        friends.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                String username = friends.getItemAtPosition(i).toString();

                final String[] values = {"0", "0"};

                if (username.substring(0, 3).equals("G: ")) {
                    String group_name = username.substring(3, username.length());

                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(group_name).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String score = (String) (dataSnapshot.child("Score").getValue());
                            String count = (String) (dataSnapshot.child("Count").getValue());
                            values[0] = score;
                            values[1] = count;
                            Snackbar.make(HomeScreen.this.findViewById(android.R.id.content), "Overall Group score: " + values[0] + "/" + values[1], Snackbar.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                } else {

                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(username).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String score = (String) (dataSnapshot.child("Score").getValue());
                            String count = (String) (dataSnapshot.child("Count").getValue());
                            values[0] = score;
                            values[1] = count;
                            Snackbar.make(HomeScreen.this.findViewById(android.R.id.content), "Overall score: " + values[0] + "/" + values[1], Snackbar.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }





                return false;
            }
        });

        // TODOgroup feature
        groupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog mBuilder = new AlertDialog.Builder(HomeScreen.this).create();
                View group_view = getLayoutInflater().inflate(R.layout.bu_group_alert, null);
                final Button doneBtn = (Button) group_view.findViewById(R.id.group_done_button);
                final EditText nameEdit = (EditText) group_view.findViewById(R.id.group_name);
                final ListView group_list = (ListView) group_view.findViewById(R.id.group_friends);

                List<String> group_friend_options = new ArrayList<String>();
                group_friend_options.clear();

                for (String s : friends_names) {
                    if (!(s.substring(0, 3).equals("G: "))) {
                        group_friend_options.add(s);
                    }
                }

                // disable both done button and list of friends
                group_list.setEnabled(false);
                doneBtn.setEnabled(false);
                doneBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mBuilder.getContext(), android.R.layout.simple_list_item_1, group_friend_options);
                group_list.setAdapter(adapter);
                mBuilder.setView(group_view);
                mBuilder.show();

                final List<String> group_recipients = new ArrayList<String>();

                // create the name for the group
                nameEdit.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        group_list.setEnabled(true);
                        doneBtn.setEnabled(true);
                        doneBtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button));
                    }
                });

                // TODO - did in plane
                // TODO make it so a user can deselect a recipient

                // add users to the group using the friend list
                group_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                        if (group_recipients.contains(group_list.getItemAtPosition(position).toString())) {
                            // change the backround back to white
                            group_list.getChildAt(position).setBackgroundColor(Color.WHITE);
                            // remove this user from the group recipients list
                            group_recipients.remove(group_list.getItemAtPosition(position).toString());
                        } else {
                            // this user is not already in the list so change their backgroun color to selected and add them to the list
                            group_list.getChildAt(position).setBackgroundColor(Color.rgb(242, 242, 242));
                            group_recipients.add(group_list.getItemAtPosition(position).toString());
                        }
                    }
                });

                // TODO - did on plane
                // TODO changing firebase to have child of members and the count of the group chat

                doneBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progressDialog = new ProgressDialog(mBuilder.getContext());
                        progressDialog.setMessage("Creating group...");
                        progressDialog.show();

                        // add the members to the firebase members child
                        for (String s : group_recipients) {
                            ref_users.child(auth.getCurrentUser().getUid()).child("Groups").child("G: " + nameEdit.getText().toString())
                                    .child("Members").child(s).setValue("").addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                        progressDialog.dismiss();
                                        mBuilder.dismiss();


                                    } else {
                                        progressDialog.dismiss();
                                        mBuilder.dismiss();
                                        Snackbar.make(HomeScreen.this.findViewById(android.R.id.content), "Unable to create group",
                                                Snackbar.LENGTH_SHORT).show();
                                    }
                                }
                            });

                            // add the group the firebase of all the other users too
                            List<String> users = new ArrayList<String>();
                            users.clear();
                            users.addAll(group_recipients);
                            // remove the user whose firebase we are adding to (should add his own name)
                            users.remove(s);
                            // add the user who created the group
                            users.add(auth.getCurrentUser().getDisplayName());
                            // place in the firebase of each of the members in the group
                            for (String user : users) {
                                ref_users.child(map_ids.get(s)).child("Groups").child("G: " + nameEdit.getText().toString())
                                        .child("Members").child(user).setValue("");

                            }

                            // add count feature
                            ref_users.child(map_ids.get(s)).child("Groups").child("G: " + nameEdit.getText().toString())
                                    .child("Count").setValue("0");



                            // add to chat
                            ref_users.child(map_ids.get(s)).child("Chats").child(nameEdit.getText().toString())
                                    .child("Count").setValue("0");

                            // add score
                            ref_users.child(map_ids.get(s)).child("Chats").child(nameEdit.getText().toString()).child("Score").setValue("0");



                        }
                        // add to my chats and add count
                        ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(nameEdit.getText().toString())
                                .child("Count").setValue("0");
                        // add count to my groups
                        ref_users.child(auth.getCurrentUser().getUid()).child("Groups").child("G: " + nameEdit.getText().toString())
                                .child("Count").setValue("0");

                        // add score to my fb
                        ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(nameEdit.getText().toString())
                                .child("Score").setValue("0");



                        // add the new group to the list of friends
                        friends_names.add("G: " + nameEdit.getText().toString());
                        arrayAdapter.notifyDataSetChanged();
                    }
                });

                // TODO - commented this out on the plane (but this works okay)
                /*
                // handle done onclick
                doneBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        progressDialog = new ProgressDialog(mBuilder.getContext());
                        progressDialog.setMessage("Creating group...");
                        progressDialog.show();
                        // for-each loop to add a child of the recipient name to the group child in fb
                        for (String s : group_recipients) {
                            ref_users.child(auth.getCurrentUser().getUid()).child("Groups").child("Group: " + nameEdit.getText().toString())
                                    .child(s).setValue("").addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        progressDialog.dismiss();
                                    }
                                }
                            });
                            mBuilder.dismiss();
                            Snackbar.make(HomeScreen.this.findViewById(android.R.id.content), nameEdit.getText().toString() + " created",
                                    Snackbar.LENGTH_SHORT).show();
                            // add the new group to the list view of friends and update the adapter
                            friends_names.add("Group: " + nameEdit.getText().toString());
                            arrayAdapter.notifyDataSetChanged();


                        }
                    }
                });
                */

            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_friend_menu, menu);

        /*MenuItem searchItem = menu.findItem(R.id.menuFriendSearch);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchItem.setIcon(ic_menu_add);
        searchView.setQueryHint("Search a username");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String s) {
                try {
                    ref_usernames.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Map<String, String> map = (HashMap<String, String>)dataSnapshot.getValue();
                            if (map.get(s) != null) {
                                if (!(friends_names.contains(s))) {
                                    friends_names.add(s);
                                    arrayAdapter.notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {


                        }
                    });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String s) {
                ref_usernames.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                        if (map.get(s) != null) {
                            if (!friends_names.contains(s)) {
                                friends_names.add(s);
                                arrayAdapter.notifyDataSetChanged();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                return false;
            }
        });*/


        final MenuItem settingsItem = menu.findItem(R.id.settingsItem);

        settingsItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                menuItem = settingsItem;
                Intent intent = new Intent(HomeScreen.this, Settings.class);
                startActivityForResult(intent, 4);
                return false;
            }
        });

        final MenuItem friendNotifItem = menu.findItem(R.id.friend_notif_item);

        if (hasFriend) {
            System.out.println("Got here - friend : ");
            friendNotifItem.setIcon(getResources().getDrawable(R.drawable.bu_add_friend_notif_item_filled_icon));
        }

        friendNotifItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                final AlertDialog mBuilder = new AlertDialog.Builder(HomeScreen.this).create();
                View fNotifView = getLayoutInflater().inflate(R.layout.bu_friend_req_alert,null);
                ListView req_list = (ListView) fNotifView.findViewById(R.id.req_list);

                final List<String> requests = new ArrayList<String>();

                final ArrayAdapter<String> adapter = new CustomFriendListAdapter(mBuilder.getContext(), requests);
                req_list.setAdapter(adapter);

                ref_users.child(auth.getCurrentUser().getUid()).child("Requests").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                        System.out.println("This is req map: " + map);
                        // for first time user
                        try {
                            if (!map.isEmpty()) {
                                friendNotifItem.setIcon(getResources().getDrawable(R.drawable.bu_add_friend_notif_item_filled_icon));
                            } else {

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        requests.clear();
                        try {
                            // if no friends at all and first time user it throws a NullPointer
                            requests.addAll(map.keySet());
                            for (String s : map.keySet()) {
                                if ((map.get(s).equalsIgnoreCase("Accepted")) || (map.get(s).equalsIgnoreCase("Denied"))) {
                                    requests.remove(s);
                                }

                                // TODO another way of adding to friends list - other way is in CustomFriendsListAdapter
                            /*if (map.get(s).equalsIgnoreCase("accepted")) {
                                friends_names.add(s);
                                arrayAdapter.notifyDataSetChanged();
                            }*/
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }


                        if (requests.isEmpty()) {
                            mBuilder.dismiss();
                            friendNotifItem.setIcon(getResources().getDrawable(R.drawable.friend_notif_icon_1));
                            Snackbar.make(HomeScreen.this.findViewById(android.R.id.content), "No new friend requests", Snackbar.LENGTH_SHORT).show();
                        }
                        adapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                // try to fix dups in friends list


                mBuilder.setView(fNotifView);
                mBuilder.show();
                return false;
            }
        });

        final MenuItem chatNotif = menu.findItem(R.id.chat_notif);

        // TODO - did this on plane

        if (containsChat) {
            System.out.println("This is hasChat: " + hasChat);
            chatNotif.setIcon(getResources().getDrawable(R.drawable.bu_chat_notif_filled_f));
        }

        chatNotif.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                final AlertDialog chatBuilder = new AlertDialog.Builder(HomeScreen.this).create();
                View chatView = getLayoutInflater().inflate(R.layout.bu_chats_alert, null);
                final ListView chat_listView = (ListView) chatView.findViewById(R.id.chats_listview);


                final ArrayAdapter<String> adapter = new CustomChatListViewAdapter(chatBuilder.getContext(), chat_inbox);
                chat_listView.setAdapter(adapter);

                chat_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Intent intent = new Intent(HomeScreen.this, RecipientChat.class);

                        // just sender name
                        String chat_name = chat_listView.getItemAtPosition(i).toString();
                        String group_name = chat_name.substring(3, chat_name.length());
                        System.out.println("This is group names: " + group_names);


                        if (group_names.contains("G: " + group_name)) {
                            intent.putExtra("Chat name", group_name);

                            intent.putExtra("Sender name", group_name);
                            intent.putExtra("Is Group", true);
                            chatBuilder.dismiss();
                            startActivityForResult(intent, 8);

                        } else {
                            // send the name of the chat
                            intent.putExtra("Chat name", chat_name);
                            intent.putExtra("Is Group", false);

                            String[] user_name_array = chat_name.split(" ");
                            sender_name = user_name_array[0];
                            System.out.println("This is sender name : " + sender_name);

                            // send the name of the sender
                            intent.putExtra("Sender name", sender_name);
                            chatBuilder.dismiss();
                            startActivityForResult(intent, 8);
                        }



                    }
                });

                chatBuilder.setView(chatView);
                chatBuilder.show();


                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    // asking user for camera permissions
    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(HomeScreen.this, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(HomeScreen.this, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(HomeScreen.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(HomeScreen.this, new String[]{permission}, requestCode);
            }
        } else {
            //Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }



    // handle back button for this activity
    @Override
    public void onBackPressed() {
        // do nothing
        finish();
        finishActivity(0);
        finishActivity(-1);
    }


}
