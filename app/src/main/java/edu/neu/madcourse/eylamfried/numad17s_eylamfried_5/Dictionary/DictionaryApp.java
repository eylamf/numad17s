package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Dictionary;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.MainActivity_main;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 2/4/17.
 */

public class DictionaryApp extends Activity {
    private ArrayList<String> words = new ArrayList<>();
    private ArrayList<String> foundList = new ArrayList<>();
    private static final String TAG = DictionaryApp.class.getSimpleName();

    public DictionaryApp() {
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);

        final Button clearButton = (Button) findViewById(R.id.clear_button);
        final Button returnButton = (Button) findViewById(R.id.return_button);
        final Button ackButton = (Button) findViewById(R.id.ack_button);
        final TextView foundWords = (TextView) findViewById(R.id.words_text);
        final EditText inputWords = (EditText) findViewById(R.id.inputs_edit);


        //when clear button is clicked, reset text values
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foundWords.setText("Words found: ");
                inputWords.setText("");
                foundList = new ArrayList<String>();
            }
        });

        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DictionaryApp.this, MainActivity_main.class);
                startActivity(intent);
                finish();

            }
        });

        ackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DictionaryApp.this, Acknowledgements.class);
                startActivity(intent);

            }
        });

        //actions based on EditText box
        inputWords.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 3) {
                    String input = inputWords.getText().toString().toLowerCase();
                    String searchFile = input.substring(0,1).toUpperCase() + input.substring(1,2).toLowerCase();
                    readFile(searchFile);
                    //if words contains the user's input, add it to a new 'found' list and to TextView
                    if (words.contains(input.toLowerCase())) {
                        //if (Collections.binarySearch(words, input.toLowerCase()) >= 0) {
                        if (!foundList.contains(input.toLowerCase())) {
                            foundList.add(input.toLowerCase());
                            foundWords.append("\n" + input);
                            Log.d(TAG, "FOUND");
                            ToneGenerator foundTone = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
                            foundTone.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200);
                        }
                    }
                }
            }
        });
    }

    /**
     * read the given txt file
     * @param file a specified txt file
     */
    public void readFile(String file) {
        BufferedReader reader;
        AssetManager assets = getResources().getAssets();
        InputStream inputStream;

        try {
            inputStream = assets.open(file);
            InputStreamReader inputReader = new InputStreamReader(inputStream);
            reader = new BufferedReader(inputReader, 1024);
            String line;

            while ((line = reader.readLine()) != null) {
                words.add(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //handle events when resumed
    protected void onResume() {
        super.onResume();

    }
    //handle events when paused
    protected void onPause() {
        super.onPause();

    }

}

