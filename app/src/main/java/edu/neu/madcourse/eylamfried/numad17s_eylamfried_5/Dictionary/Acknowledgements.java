package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Dictionary;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 2/4/17.
 */

public class Acknowledgements extends AppCompatActivity {

    TextView ackText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_knowledgement);

        ackText = (TextView) findViewById(R.id.ack_text);


        String acknowledgement =
                "Strategy: I parsed the original 'wordlist.txt' file into sub text files. The text files are " +
                        "organized according to the first two characters of words. So for examples, all words " +
                        "starting with 'aa' are in the Aa.txt file, all words starting with 'ab' are in the " +
                        "Ab.txt file. Then I created a 'read(String s)' function. The function adds the words in the " +
                        "specified file to an ArrayList. The file is specified by taking the first two characters of " +
                        "the user's input word. 'read(String s)' then reads only the file with the same two characters." +
                        " If a word is found, it adds it to the TextView and to a 'found' list in order to avoid duplicates."
                        + "\n" + "\n" + "Reading Files: I used a YouTube tutorial in order to re-familiarize " +
                        "myself with " +
                        "reading txt files. The URL I used is https://www.youtube.com/watch?v=SlWnnkbwjFQ&t=351s";

        ackText.setText(acknowledgement);
    }
}



