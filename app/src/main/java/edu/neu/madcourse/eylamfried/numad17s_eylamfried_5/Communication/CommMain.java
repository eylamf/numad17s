package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Communication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.MainActivity_main;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 3/26/17.
 */

public class CommMain extends AppCompatActivity{

    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;

    private String emailStr;
    private String passwordStr;

    private FirebaseAuth.AuthStateListener authStateListener;

    private DatabaseReference root;

    private static final String TAG = CommMain.class.getSimpleName();

    private FirebaseUser firebaseUser;
    private User user;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comm_main);
        setTitle("Communication");




        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);



        //TODO signs user out each time - can get rid of this
        firebaseAuth.signOut();

        // Users root in db
        root = FirebaseDatabase.getInstance().getReference().child("Users");

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    startActivity(new Intent(CommMain.this, CommMainMenu.class));
                    System.out.println(firebaseAuth.getCurrentUser().getEmail().toString());
                }
            }
        };

        firebaseUser = firebaseAuth.getCurrentUser();



        //f
        //login material
        Button mLogin = (Button) findViewById(R.id.log_in_b);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog mBuilder = new AlertDialog.Builder(CommMain.this).create();
                //reference the alertDialog layout as a view in order to get attributes
                View v = getLayoutInflater().inflate(R.layout.login_view, null);
                final EditText mEmail = (EditText) v.findViewById(R.id.login_email);
                final EditText mPassword = (EditText) v.findViewById(R.id.login_pass);
                Button mLoginBtn = (Button) v.findViewById(R.id.login_loginButton);


                mLoginBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //System.out.println(user.getEmail().toString());
                        emailStr = mEmail.getText().toString();
                        passwordStr = mPassword.getText().toString();
                        //user = new User(firebaseUser.getDisplayName(), 0);




                        if (TextUtils.isEmpty(emailStr) || TextUtils.isEmpty(passwordStr)) {
                            Toast.makeText(CommMain.this, "Please fill out each field", Toast.LENGTH_SHORT).show();
                        } else {

                            firebaseAuth.signInWithEmailAndPassword(emailStr, passwordStr).addOnCompleteListener(
                                    new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {

                                            if(!task.isSuccessful()) {
                                                Toast.makeText(CommMain.this, "Sign in error, try again", Toast.LENGTH_SHORT).show();

                                            } else {
                                                Toast.makeText(CommMain.this, "Login successful", Toast.LENGTH_SHORT).show();
                                                mBuilder.dismiss();

                                                //user.setStatus("online");
                                            }

                                        }
                                    }
                            );

                        }
                    }
                });

                //set the view for the layout
                mBuilder.setView(v);
                AlertDialog dialog = mBuilder;
                dialog.show();
            }
        });


        Button mSignup = (Button) findViewById(R.id.sign_up_b);
        mSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog mBuilder2 = new AlertDialog.Builder(CommMain.this).create();
                //reference the alertDialog layout as a view in order to get attributes
                View mView = getLayoutInflater().inflate(R.layout.sign_up_view, null);
                final EditText mEmail = (EditText) mView.findViewById(R.id.signup_email);
                final EditText mUsername = (EditText) mView.findViewById(R.id.signup_username);
                final EditText mPassword = (EditText) mView.findViewById(R.id.signup_pass);
                Button mSignupBtn = (Button) mView.findViewById(R.id.signup_signupButton);
                mSignupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(!(mEmail.getText().toString().isEmpty()) && !(mUsername.getText().toString().isEmpty()) &&
                                !(mPassword.getText().toString().isEmpty())) {


                            firebaseAuth.createUserWithEmailAndPassword(mEmail.getText().toString(), mPassword.getText().toString()).addOnCompleteListener(
                                    new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {

                                            if(task.isSuccessful()) {
                                                //successful
                                                Toast.makeText(CommMain.this, "Sign up successful", Toast.LENGTH_SHORT).show();
                                                mBuilder2.dismiss();


                                                User new_user = new User(mUsername.getText().toString(), 0);
                                                new_user.setStatus("online");


                                                //DatabaseReference user_name = mDatabase.child("name");
                                                root.push().setValue(new_user);


                                                //update the User's display name
                                                FirebaseUser user = firebaseAuth.getCurrentUser();

                                                UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                                                        .setDisplayName(mUsername.getText().toString()).build();
                                                int[] arr = {1, 3};



                                                user.updateProfile(profileUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Log.d(TAG, "User profile updated");
                                                        }
                                                    }
                                                });




                                            } else {
                                                Toast.makeText(CommMain.this, "Could not register. Please try again", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }
                            );

                        } else {
                            Toast.makeText(CommMain.this, "Please fill out each field", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                mBuilder2.setView(mView);
                AlertDialog dialog2 = mBuilder2;
                dialog2.show();
            }
        });



    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

}
