package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

public class LoginPage extends AppCompatActivity {


    // database and refs
    private FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
    private DatabaseReference ref_users;
    private DatabaseReference ref_usernames;

    // auth
    private FirebaseAuth auth;

    // buttons
    private Button login_btn;
    private Button signUp_btn;

    // progress dialog
    private ProgressDialog progressDialog;

    // firebase storage
    private FirebaseStorage mStorage = FirebaseStorage.getInstance();
    private StorageReference storage_users;


    // use this view for snackbars after AlertDialogs
    private View page_view;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_page);
        setTitle("BuddyUp");

        // init firebase storage
        storage_users = mStorage.getReference().child("BU Users");

        // init firebase authentication
        auth = FirebaseAuth.getInstance();

        // init firebase database
        ref_users = mDatabase.getReference().child("BU Users");
        ref_usernames = mDatabase.getReference().child("BU Usernames");

        // init buttons
        login_btn = (Button) findViewById(R.id.login_button);
        signUp_btn = (Button) findViewById(R.id.signUp_button);



        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // set the page view to this view
                page_view = view;

                // create alertdialog for login
                final AlertDialog mBuilder = new AlertDialog.Builder(LoginPage.this).create();
                View login_view = getLayoutInflater().inflate(R.layout.activity_login_alert, null);
                final Button login_b = (Button) login_view.findViewById(R.id.login_b);
                final EditText login_email = (EditText) login_view.findViewById(R.id.login_email_value);
                final EditText login_pass = (EditText) login_view.findViewById(R.id.login_pass_value);
                Button forgot_pass = (Button) login_view.findViewById(R.id.forgot_pass_button);

                progressDialog = new ProgressDialog(mBuilder.getContext());

                // disable login button
                //login_b.setBackgroundColor(Color.rgb(65, 13, 122));
                login_b.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));
                login_b.setEnabled(false);
                // text watcher for login alert
                login_pass.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (TextUtils.isEmpty(login_pass.getText().toString())){
                            //login_b.setBackgroundColor(Color.rgb(65, 13, 122));
                            login_b.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));
                            login_b.setEnabled(false);
                        }

                        if (login_pass.getText().toString().length() < 6) {
                            //login_b.setBackgroundColor(Color.rgb(65, 13, 122));
                            login_b.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));
                            login_b.setEnabled(false);
                        }

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (!TextUtils.isEmpty(login_email.getText().toString()) && !TextUtils.isEmpty(login_pass.getText().toString()) &&
                                login_pass.getText().toString().length() >= 6) {
                            //login_b.setBackgroundColor(Color.rgb(117, 26, 214));
                            login_b.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button));
                            login_b.setEnabled(true);
                        }
                    }
                });


                login_b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String email = login_email.getText().toString();
                        String pass = login_pass.getText().toString();
                        // check if the inputs are not blank
                        if (!(TextUtils.isEmpty(email)) && !(TextUtils.isEmpty(pass))) {
                            // init prog dialog
                            progressDialog.setMessage("Initializing profile...");
                            progressDialog.show();
                            // sign in user

                            if (checkNetwork()) {
                                auth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // bring user to home activity
                                            progressDialog.dismiss();
                                            mBuilder.dismiss();
                                            Intent intent = new Intent(LoginPage.this, HomeScreen.class);
                                            startActivityForResult(intent, 0);
                                        } else {
                                            // error - no user found
                                            progressDialog.dismiss();
                                            Toast.makeText(mBuilder.getContext(), "No user found. Try again.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            } else {
                                progressDialog.dismiss();
                                mBuilder.dismiss();
                                Snackbar.make(LoginPage.this.findViewById(android.R.id.content), "Unable to login: No network connection", Snackbar.LENGTH_SHORT).show();
                            }

                        } else {
                            // inputs are blank - notify the user
                            Toast.makeText(mBuilder.getContext(), "Please fill out each input", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

                forgot_pass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog builder2 = new AlertDialog.Builder(mBuilder.getContext()).create();
                        View forgotView = getLayoutInflater().inflate(R.layout.bu_forgot_alert, null);
                        final EditText forgot_email = (EditText) forgotView.findViewById(R.id.forgot_email);
                        Button send_code = (Button) forgotView.findViewById(R.id.forgot_button);

                        send_code.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!forgot_email.getText().toString().isEmpty()) {
                                    auth.sendPasswordResetEmail(forgot_email.getText().toString());
                                }
                            }
                        });

                        builder2.setView(forgotView);
                        builder2.show();
                    }
                });
                // show alert dialog
                mBuilder.setView(login_view);
                mBuilder.show();
            }
        });

        signUp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // link the page_view
                page_view = view;
                // alert dialog for sign up
                final AlertDialog mBuilder = new AlertDialog.Builder(LoginPage.this).create();
                View signUp_view = getLayoutInflater().inflate(R.layout.activity_signup_alert, null);
                final Button signup_b = (Button) signUp_view.findViewById(R.id.sign_b);
                // init views in alert dialog
                final EditText sign_email = (EditText) signUp_view.findViewById(R.id.sign_email_value);
                final EditText sign_username = (EditText) signUp_view.findViewById(R.id.sign_username_value);
                final EditText sign_pass = (EditText) signUp_view.findViewById(R.id.sign_pass_value);

                // disable button
                signup_b.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));
                //signup_b.setBackgroundColor(Color.rgb(65, 13, 122));
                signup_b.setEnabled(false);

                progressDialog = new ProgressDialog(mBuilder.getContext());

                // set and show builder
                mBuilder.setView(signUp_view);
                mBuilder.show();


                // text watcher for sign up
                sign_pass.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        // make sure it is not empty
                        // TODO (REDUNDANT - can delete this)
                        if (TextUtils.isEmpty(sign_pass.getText().toString())) {
                            signup_b.setBackgroundColor(Color.rgb(65, 13, 122));
                            signup_b.setEnabled(false);
                        }
                        // make sure password is longer than 6 chars
                        if (sign_pass.getText().toString().length() < 6) {
                            //signup_b.setBackgroundColor(Color.rgb(65, 13, 122));
                            signup_b.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));
                            signup_b.setEnabled(false);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (!TextUtils.isEmpty(sign_email.getText().toString()) && !TextUtils.isEmpty(sign_username.getText().toString())
                                && !TextUtils.isEmpty(sign_pass.getText().toString()) &&
                                sign_pass.getText().toString().length() >= 6) {
                            //signup_b.setBackgroundColor(Color.rgb(117, 26, 214));
                            signup_b.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button));
                            signup_b.setEnabled(true);
                        }
                    }
                });

                signup_b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // convert EditText values to strings
                        final String email = sign_email.getText().toString();
                        final String pass = sign_pass.getText().toString();
                        final String username = sign_username.getText().toString();

                        if (!TextUtils.isEmpty(sign_email.getText().toString()) && !TextUtils.isEmpty(sign_pass.getText().toString()) &&
                                !TextUtils.isEmpty(sign_username.getText().toString())) {
                            // init progressDialog
                            progressDialog.setMessage("Creating user...");
                            progressDialog.show();

                            if (checkNetwork()) {
                                auth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // dismiss the progressDialog
                                            progressDialog.dismiss();

                                            // dismiss the alert dialog page
                                            mBuilder.dismiss();
                                            // init user's display name
                                            UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                                                    .setDisplayName(username).build();
                                            auth.getCurrentUser().updateProfile(profileUpdate);
                                            // add a user to the "users" child in database
                                            ref_users.child(auth.getCurrentUser().getUid()).child("Username").setValue(username);
                                            ref_users.child(auth.getCurrentUser().getUid()).child("Email").setValue(email);
                                            ref_users.child(auth.getCurrentUser().getUid()).child("Password").setValue(pass);


                                            // add username to firebase username ref
                                            ref_usernames.child(username).setValue(auth.getCurrentUser().getUid());

                                            // init user's storage
                                            ByteArrayOutputStream boas = new ByteArrayOutputStream();
                                            storage_users.child(username).child("Blank File").putBytes(boas.toByteArray());

                                            // toast to notify user their account was made
                                            Toast.makeText(LoginPage.this, "Sign up successful", Toast.LENGTH_SHORT).show();
                                        } else {
                                            progressDialog.dismiss();
                                            Toast.makeText(LoginPage.this, "An account is already linked to this email", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            } else {
                                progressDialog.dismiss();
                                mBuilder.dismiss();
                                Snackbar.make(LoginPage.this.findViewById(android.R.id.content), "Unable to create user: No network connection", Snackbar.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(mBuilder.getContext(), "Please fill out each input", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }
        });


    }

    public boolean checkNetwork() {
        boolean status = false;

        try {
            ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = manager.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = manager.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    status = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return status;
    }


}
