package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle;


import android.media.MediaPlayer;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import android.widget.TextView;


import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 2/14/17.
 */


public class ScroggleGameAct extends FragmentActivity {
    public static String KEY_RESTORE = "key_restore";
    public static String PREF_RESTORE = "pref_restore";

    public ScroggleFrag scroggleFrag;


    private int phaseOnePoints = 0;
    private int phaseTwoPoints = 0;
    private String gameData = "";

    private int phase = 1;
    private Boolean restore = false;
    public static Boolean isResume = false;
    private String phaseTwoWord;
    public TextView scoreView;
    public TextView timeView;

    private String hello;
    public String number_of_players = "2";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //number_of_players = getIntent().getExtras().get("num_players").toString();





        Bundle b = this.getIntent().getExtras();
        if (b != null) {
            phaseTwoWord = "";
            phase = 2;
            gameData = b.getString("gameData");
        }

        setContentView(R.layout.game_activity_scroggle);
        scroggleFrag = (ScroggleFrag) getFragmentManager()
                .findFragmentById(R.id.fragment_scroggle);
        scroggleFrag.setAmountPlayer("2");


        scoreView = (TextView) findViewById(R.id.score_text);
        timeView = (TextView) findViewById(R.id.timer);
        //hello = getIntent().getExtras().get("num_players").toString();


        boolean restore = getIntent().getBooleanExtra(KEY_RESTORE, false);

        if (restore) {
            String gameData = getPreferences(MODE_PRIVATE)
                    .getString(PREF_RESTORE, null);
            if (gameData != null) {
                scroggleFrag.putState(gameData);
            }

        }


    }

    public void updateScore() {
        scoreView.setText("Score: " + Integer.toString(scroggleFrag.score));

    }

    @Override
    public void onPause() {
        super.onPause();
        scroggleFrag.mHandler.removeCallbacks(null);
        scroggleFrag.mediaPlayer.stop();
        scroggleFrag.mediaPlayer.reset();
        scroggleFrag.mediaPlayer.release();
        scroggleFrag.countDownTimer.cancel();
        if (isResume) {
            isResume = false;
        }
        //String gameData = scroggleFrag.getState();
        getPreferences(MODE_PRIVATE).edit()
                .putString(PREF_RESTORE, gameData)
                .commit();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!isResume) {
            isResume = true;
            if (scroggleFrag.timeRemaning > 0) {
                scroggleFrag.countDownTimer.start();
            } else {
                scroggleFrag.countDownTimer.start();
            }
        }
        scroggleFrag.mediaPlayer = MediaPlayer.create(this, R.raw.scroggle_gamemusic);
        scroggleFrag.mediaPlayer.start();
        if (phase == 2) {
            scoreView.setText("Score " + scroggleFrag.score);
        } else {
            scoreView.setText("Score " + scroggleFrag.score);
        }

    }



}
