package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

import static java.lang.Integer.parseInt;

/**
 * Created by eylamfried on 4/21/17.
 */

public class RecipientChat extends AppCompatActivity {

    // init firebase
    private FirebaseAuth auth;
    private FirebaseDatabase mDatabase;
    private DatabaseReference ref_users;
    private DatabaseReference ref_usernames;
    private DatabaseReference ref_chats;


    // image views
    private ImageView mImage1;
    private ImageView mImage2;
    private ImageView mImage3;

    //senders name
    private String sender_name;

    private String chat_name;

    // checker for
    private boolean checker = false;
    // checker for score change
    private boolean checker2 = false;

    // button for viewing captions
    private Button viewCaps;

    // correct ans
    private String correctCap = "";
    private String userChoice = "";

    // init the player's score
    private String curr_score = "1";

    // testing current score
    private String chat_score = "";

    // map of the user ids
    Map<String, String> user_ids = new HashMap<>();

    private List<String> sender_names = new ArrayList<>();

    // GROUP FEATURE
    private boolean isGroup = false;

    private String groupCount = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bu_recipient_chat_activity);

        chat_name = this.getIntent().getExtras().get("Chat name").toString();
        sender_name = this.getIntent().getExtras().get("Sender name").toString();
        isGroup = (boolean) this.getIntent().getExtras().get("Is Group");


        // set the title of the activity
        setTitle("From " + sender_name);

        // set firebase
        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        ref_users = mDatabase.getReference().child("BU Users");
        ref_usernames = mDatabase.getReference().child("BU Usernames");
        ref_chats = mDatabase.getReference().child("BU Chats");


        // set up the user id map
        ref_usernames.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, String> map = (HashMap<String, String>)dataSnapshot.getValue();
                // create the user id map
                user_ids.clear();
                user_ids.putAll(map);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mImage1 = (ImageView) findViewById(R.id.imageViewR1);
        mImage2 = (ImageView) findViewById(R.id.imageViewR2);
        mImage3 = (ImageView) findViewById(R.id.imageViewR3);

        viewCaps = (Button) findViewById(R.id.see_caps);

        final String[] img_data1 = new String[1];
        final String[] img_data2 = new String[1];
        final String[] img_data3 = new String[1];



        ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(sender_name).child(chat_name).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("Sender name : "  +sender_name);
                System.out.println("Chat name : " + chat_name);
                try {
                    Map<String, String> map = (HashMap<String, String>)dataSnapshot.getValue();
                    img_data1[0] = map.get("Image 1");
                    img_data2[0] = map.get("Image 2");
                    img_data3[0] = map.get("Image 3");

                    System.out.println("This is the map of imgs: " + map);
                    System.out.println(img_data1[0]);
                    System.out.println(img_data2[0]);
                    System.out.println(img_data3[0]);
                    System.out.println(img_data1[0].equalsIgnoreCase(img_data2[0]));

                    byte[] data = Base64.decode(img_data1[0], Base64.NO_WRAP);
                    byte[] data2 = Base64.decode(img_data2[0], Base64.NO_WRAP);
                    byte[] data3 = Base64.decode(img_data3[0], Base64.NO_WRAP);



                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inScaled = false;
                    Bitmap bm = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                    System.out.println("This is the bitmap : " + bm);
                    mImage1.setImageBitmap(bm);
                    Bitmap bm2 = BitmapFactory.decodeByteArray(data2, 0, data2.length, options);
                    mImage2.setImageBitmap(bm2);
                    Bitmap bm3 = BitmapFactory.decodeByteArray(data3, 0, data3.length, options);
                    mImage3.setImageBitmap(bm3);

                    checker = true;
                    System.out.println(checker);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // set the score value
        ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(sender_name).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String score = (String) dataSnapshot.child("Score").getValue();
                System.out.println("This is the current chat score : " + score);
                chat_score = score;
                System.out.println("This is the chat_score value : " + chat_score);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // view the captions
        viewCaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // set the  chat status to opened
                ref_chats.child(auth.getCurrentUser().getDisplayName()).child(chat_name).setValue("Opened");
                if (checker2) {
                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(sender_name).child("Score").setValue(curr_score);
                    System.out.println("Got to checker 2 code");
                }

                // alert for caps
                final AlertDialog mBuilder = new AlertDialog.Builder(RecipientChat.this).create();
                View capsView = getLayoutInflater().inflate(R.layout.bu_recipient_view_caps_alert, null);
                Button finished = (Button) capsView.findViewById(R.id.finished_button);
                final ListView caps_list = (ListView) capsView.findViewById(R.id.caps_listView);

                final List<String> captions = new ArrayList<String>();
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mBuilder.getContext(), android.R.layout.simple_list_item_1, captions);
                caps_list.setAdapter(adapter);

                // import the list of captions from firebase
                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(sender_name).child(chat_name).child("Captions").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Map<String, String> map = (HashMap<String, String>)dataSnapshot.getValue();
                        System.out.println("This is the map : " + map);
                        captions.clear();
                        captions.addAll(map.values());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                // get the correct captions answer from firebase
                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(sender_name).child(chat_name).child("Correct").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String ans = (String) dataSnapshot.getValue();
                        correctCap = ans;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                // init an array of booleans that correlates to the listview items (for on click)
                final boolean[] buttons = new boolean[captions.size()];
                final HashMap<Integer, Boolean> capMap = new HashMap<Integer, Boolean>();

                for (int i = 0; i < buttons.length; i++) {
                    buttons[i] = false;
                    capMap.put(i, false);
                }


                final List<Integer> prev = new ArrayList<>();


                caps_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        // if nothing has been selected, change background color
                        if (prev.isEmpty()) {
                            prev.add(i);
                            caps_list.getChildAt(i).setBackgroundColor(Color.rgb(240, 240, 240));
                            userChoice = caps_list.getItemAtPosition(i).toString();
                        } else {
                            caps_list.getChildAt(prev.get(0)).setBackgroundColor(Color.WHITE);
                            prev.clear();
                            prev.add(i);
                            caps_list.getChildAt(i).setBackgroundColor(Color.rgb(240, 240, 240));
                            userChoice = caps_list.getItemAtPosition(i).toString();
                        }




                    }
                });

                finished.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (userChoice.equalsIgnoreCase(correctCap)) {
                            //Toast.makeText(mBuilder.getContext(), "CORRECT!", Toast.LENGTH_SHORT).show();

                            /*ref_users.child(auth.getCurrentUser().getUid()).child("Chats").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Map<String, String> map = (HashMap<String, String>)dataSnapshot.child(sender_name).getValue();
                                    int score = parseInt(map.get("Score"));
                                    curr_score = Integer.toString(score + 1);
                                    checker2 = true;
                                    System.out.println("This is the current score : " + map.get("Score"));
                                    System.out.println("score changed");

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });*/

                            // alert for correct
                            final AlertDialog correctAlert = new AlertDialog.Builder(RecipientChat.this).create();
                            View correctView = getLayoutInflater().inflate(R.layout.bu_correct_alert, null);
                            TextView score_text = (TextView) correctView.findViewById(R.id.correct_score);
                            Button reply_btn = (Button) correctView.findViewById(R.id.reply_button);
                            Button home_btn = (Button) correctView.findViewById(R.id.back_home_button);


                            // increment score by 1
                            int sc = parseInt(chat_score);
                            chat_score = Integer.toString(sc + 1);
                            ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(sender_name).child("Score").setValue(chat_score);
                            System.out.println("The new chat_score value is : " + chat_score);

                            // set the score view
                            score_text.setText("Score: " + chat_score);
                            // button on-clicks
                            home_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mBuilder.dismiss();
                                    correctAlert.dismiss();
                                    finish();
                                }
                            });

                            if (isGroup) {
                                reply_btn.setVisibility(View.INVISIBLE);
                            }
                            reply_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(RecipientChat.this, CaptureActivity.class);
                                    /*if (isGroup) {

                                        intent.putExtra("Group name", sender_name);
                                        intent.putExtra("Is group", true);
                                        intent.putExtra("Group count", groupCount);
                                    } else {
                                        intent.putExtra("recipient name", sender_name);
                                    }
                                    */

                                    System.out.println("this is the sender name : " + sender_name);


                                    intent.putExtra("recipient name", sender_name);


                                    mBuilder.dismiss();
                                    correctAlert.dismiss();
                                    RecipientChat.this.finish();
                                    startActivity(intent);
                                }
                            });

                            correctAlert.setView(correctView);
                            correctAlert.show();


                        } else {
                            //Toast.makeText(mBuilder.getContext(), "Sorry, that's incorrect", Toast.LENGTH_SHORT).show();
                            final AlertDialog wrongAlert = new AlertDialog.Builder(RecipientChat.this).create();
                            View wrongView = getLayoutInflater().inflate(R.layout.bu_wrong_alert, null);
                            TextView correctAns = (TextView) wrongView.findViewById(R.id.correct_ans);
                            Button reply_btn = (Button) wrongView.findViewById(R.id.reply_button);
                            Button home_btn = (Button) wrongView.findViewById(R.id.back_home_button);

                            correctAns.setText("\"" + correctCap + "\"");

                            // take user back to home screen
                            home_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mBuilder.dismiss();
                                    wrongAlert.dismiss();
                                    finish();
                                }
                            });
                            reply_btn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(RecipientChat.this, CaptureActivity.class);
                                    intent.putExtra("recipient name", sender_name);
                                    mBuilder.dismiss();
                                    wrongAlert.dismiss();
                                    RecipientChat.this.finish();
                                    startActivity(intent);
                                }
                            });

                            wrongAlert.setView(wrongView);
                            wrongAlert.show();

                        }
                        /*// delete the data on my firebase
                        ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child("3").removeValue();
                        // delete the data on the senders firebase
                        ref_users.child(user_ids.get(sender_name)).child("Chats").child("3").removeValue();
                        System.out.println(ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child("3"));
                        */



                        //mBuilder.dismiss();
                        //finish();

                    }
                });

                mBuilder.setView(capsView);
                mBuilder.show();

            }
        });







    }




}

