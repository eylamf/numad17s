package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Communication;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eylamfried on 3/16/17.
 */

@IgnoreExtraProperties
public class User {

    private String user_name;
    private int score;
    private String status;
    private List<String> wordsInGame;
    private List<String> letterOrder;

    public User() {}

    public User(String username, int score) {
        this.user_name = username;
        this.score = score;
        this.status = "offline";
        this.wordsInGame = new ArrayList<>();
        this.letterOrder = new ArrayList<>();
    }

    /**
     * getter for username
     *
     */
    public String getUsername() {
        return this.user_name;
    }

    /**
     * getter for score
     *
     */
    public int getScore() {
        return this.score;
    }

    /**
     * getter for status
     */
    public String getStatus() {
        return this.status;
    }

    //set the status
    public void setStatus(String s) {
        this.status = s;
    }

    //get and set words
    public void setWords(List<String> words) {
        this.wordsInGame = words;
    }

    public List<String> getWords() {
        return this.wordsInGame;
    }



}
