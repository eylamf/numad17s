package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Comm.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by eylamfried on 3/5/17.
 */

public class WordGameInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = WordGameInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        //get updated instanceID token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        //if you want to send messages to this application instance
        //or manage this apps subscriptions on the server side,
        //send the Instance ID token to your app server
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        //implement this method to send token to your app server
    }
}
