package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.TwoPlayerScroggle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 4/5/17.
 */

public class TwoPlayerHowTo extends AppCompatActivity{

    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceData) {
        super.onCreate(savedInstanceData);
        setContentView(R.layout.two_player_how_to);

        text = (TextView) findViewById(R.id.two_player_hot_to_text);
        text.setText("It's easy!\n" +
                "1) Press the Two Player Scroggle button.\n" +
                "2) Like your score? Select a friend to challenge by pressing\n" +
                "the Friends button and then select a user.\n" +
                "3) Send the challenge and wait for them to play!\n" +
                "4) If you want to check whether you have received a challenge invitation,\n" +
                "press the Check Challenges button!");

    }
}
