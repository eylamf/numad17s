package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.TwoPlayerScroggle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 4/5/17.
 */

public class TwoPlayerAck extends AppCompatActivity{

    private TextView ack_text;

    @Override
    protected void onCreate(Bundle savedInstaceData) {
        super.onCreate(savedInstaceData);
        setContentView(R.layout.activity_2_player_ack);
        setTitle("Acknowledgements");
        ack_text = (TextView) findViewById(R.id.two_player_ack_text);

        ack_text.setText("I used a couple of Firebase tutorial videos in order to further understand how \n" +
                "DatatSnapshots work. I had to use many DataSnapshots in order to extract\n" +
                "data from each user on Firebase.\n" +
                "Link: https://www.youtube.com/watch?v=2pOCfKYO5Ao\n" +
                "I also used the Firebase documents in order to learn how to organize data in Firebase.\n" +
                "Link: https://firebase.google.com/docs/reference/android/com/google/firebase/database/DataSnapshot\n" +
                "I also used the link below to check whether the user has Network availability or not.\n" +
                "Link: https://www.youtube.com/watch?v=JGCgDppv4I8" );
    }


}
