package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle.ScroggleGameAct;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.TwoPlayerScroggle.TwoPlayerMain;

import static java.lang.Integer.parseInt;

/**
 * Created by eylamfried on 4/10/17.
 */

public class BuddyUpHome extends AppCompatActivity {

    // firebase
    private StorageReference mStorage;
    private FirebaseAuth auth;
    private FirebaseDatabase mDatabase;
    private DatabaseReference ref_users;
    private DatabaseReference ref_usernames;

    private DatabaseReference ref_chats;

    private Button cam_button;
    private ImageView mImageView;
    private ImageView mImageView1;
    private ImageView mImageView2;

    private static final int REQUEST_IMAGE_CAPTURE = 1;

    private ProgressDialog progressDialog;

    private int counter;

    // array of images
    private List<ImageView> images = new ArrayList<>();

    private String title_of_page;

    private static int camCode = 0x5;

    // save the captions for the alertDialog
    private List<String> saved_captions = new ArrayList<>();

    // testing
    private byte[] image_data;
    private byte[] image_data2;
    private byte[] image_data3;

    // map of user ids
    private Map<String, String> user_ids = new HashMap<>();

    private int chatCount;


    // GROUP FEATURE
    private boolean isGroup = false;
    private int groupCount = 0;
    private String groupName = "";
    private String group_mems;
    private List<String> groupMembers = new ArrayList<>();






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buddyup_home);
        askForPermission(Manifest.permission.CAMERA, camCode);

        // init firebase
        auth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance();
        ref_users = mDatabase.getReference().child("BU Users");
        ref_usernames = mDatabase.getReference().child("BU Usernames");
        ref_chats = mDatabase.getReference().child("BU Chats");


        // get extras
        try {
            title_of_page = this.getIntent().getExtras().get("Recipient name").toString();
            chatCount = parseInt(this.getIntent().getExtras().get("Chat count").toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // get group extras
        try {
            groupCount = parseInt(this.getIntent().getExtras().get("Group count").toString());
            groupName = this.getIntent().getExtras().get("Group name").toString();

            System.out.println("This is the group name : 100 + " + groupName);

            ref_users.child(auth.getCurrentUser().getUid()).child("Groups").child("G: " + groupName).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    try {
                        Map<String, String> map = (HashMap<String, String>) dataSnapshot.child("Members").getValue();
                        System.out.println("This is the members map 3: " + map);
                        System.out.println("This is the members keyset " + map.keySet());
                        groupMembers.clear();
                        groupMembers.addAll(map.keySet());
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            //group_mems = this.getIntent().getExtras().get("Group members").toString();
            //groupMembers = group_mems.split(" ");
            isGroup = (boolean) this.getIntent().getExtras().get("Is group");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isGroup) {
            setTitle("To " + groupName);
        } else {
            setTitle("To " + title_of_page);
        }


        counter = 0;



        // create a map of the user ids
        ref_usernames.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{
                    Map<String, String> map = (HashMap<String, String>)dataSnapshot.getValue();
                    System.out.println("This is map : " + map);
                    user_ids.putAll(map);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        cam_button = (Button) findViewById(R.id.camera_button);
        mImageView = (ImageView) findViewById(R.id.imageView);

        // other images
        mImageView1 = (ImageView) findViewById(R.id.imageView1);
        mImageView2 = (ImageView) findViewById(R.id.imageView2);

        try {
            image_data = (byte[]) this.getIntent().getExtras().get("Photo");
            image_data2 = (byte[]) this.getIntent().getExtras().get("Photo2");
            image_data3 = (byte[]) this.getIntent().getExtras().get("Photo3");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            Bitmap bm = BitmapFactory.decodeByteArray(image_data, 0, image_data.length, options);
            mImageView.setImageBitmap(bm);
            Bitmap bm2 = BitmapFactory.decodeByteArray(image_data2, 0, image_data2.length, options);
            mImageView1.setImageBitmap(bm2);
            Bitmap bm3 = BitmapFactory.decodeByteArray(image_data3, 0, image_data3.length, options);
            mImageView2.setImageBitmap(bm3);
            System.out.println("This is image data " + image_data);
            cam_button.setText("Write Captions");

        } catch (Exception e) {
            e.printStackTrace();
        }

        progressDialog = new ProgressDialog(BuddyUpHome.this);



        cam_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cam_button.getText().toString().equalsIgnoreCase("Capture")){
                    try {
                    /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);*/

                        Intent intent = new Intent(BuddyUpHome.this, CaptureActivity.class);
                        startActivityForResult(intent, 5);
                        finish();

                    } catch (SecurityException e) {
                        askForPermission(Manifest.permission.CAMERA, camCode);
                        e.printStackTrace();
                    }
                } else {
                    // builder for writing captions
                    final AlertDialog mBuilder = new AlertDialog.Builder(BuddyUpHome.this).create();
                    View captionsView = getLayoutInflater().inflate(R.layout.bu_write_caps_alert, null);
                    final EditText caption_edit = (EditText) captionsView.findViewById(R.id.caption_edit);
                    final Button caption_add = (Button) captionsView.findViewById(R.id.caption_add);
                    final ListView caption_listview = (ListView) captionsView.findViewById(R.id.caption_list);
                    final Button caption_send = (Button) captionsView.findViewById(R.id.caption_send);

                    final boolean[] correctSelected = {false};
                    final int[] correct = new int[1];
                    final String[] correctString = new String[1];

                    // the chat count
                    final int[] count = new int[1];

                    // disable send button until atleast 2 captions are added
                    caption_send.setEnabled(false);
                    caption_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));

                    mBuilder.setView(captionsView);
                    mBuilder.show();
                    // init an empty array of captions
                    final List<String> captions = new ArrayList<String>();
                    //captions.addAll(saved_captions);
                    // init adapter
                    final ArrayAdapter<String> adapter = new ArrayAdapter<String>(mBuilder.getContext(), android.R.layout.simple_list_item_1, captions);
                    // set the adapter for the list view
                    caption_listview.setAdapter(adapter);
                    // onclick for the + button
                    caption_add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!(TextUtils.isEmpty(caption_edit.getText().toString()))) {
                                captions.add(caption_edit.getText().toString());
                                //saved_captions = captions;
                                adapter.notifyDataSetChanged();
                                caption_edit.getText().clear();
                                if (captions.size() >= 2 && captions.size() <= 4) {
                                    caption_send.setEnabled(true);
                                    caption_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button));
                                } else if (captions.size() > 4) {
                                    caption_send.setEnabled(false);
                                    caption_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));
                                }
                            }
                        }
                    });

                    final List<Integer> prev = new ArrayList<>();

                    caption_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            if (prev.isEmpty()) {
                                prev.add(i);
                                caption_listview.getChildAt(i).setBackgroundColor(Color.rgb(240,240,240));
                                correctString[0] = caption_listview.getItemAtPosition(i).toString();
                            } else {
                                caption_listview.getChildAt(prev.get(0)).setBackgroundColor(Color.WHITE);
                                prev.clear();
                                prev.add(i);
                                caption_listview.getChildAt(i).setBackgroundColor(Color.rgb(240,240,240));
                                correctString[0] = caption_listview.getItemAtPosition(i).toString();
                            }
                        }

                    });

                    /*
                    caption_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                            if (correctSelected[0] == false){
                                caption_listview.getChildAt(i).setBackgroundColor(Color.rgb(242, 242, 242));
                                correctSelected[0] = true;
                                correct[0] = i;
                                correctString[0] = caption_listview.getItemAtPosition(i).toString();
                            }
                            // open alertDialog to either delete option or replace correct one
                            caption_listview.getChildAt(i).setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View view) {
                                    final AlertDialog mBuilder2 = new AlertDialog.Builder(mBuilder.getContext()).create();
                                    View v = getLayoutInflater().inflate(R.layout.bu_caption_long_alert, null);
                                    Button trash = (Button) v.findViewById(R.id.bu_trash);
                                    Button replace = (Button) v.findViewById(R.id.bu_replace);

                                    mBuilder2.setView(v);
                                    mBuilder2.show();
                                    trash.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            captions.remove(caption_listview.getItemAtPosition(i).toString());
                                            adapter.notifyDataSetChanged();
                                            correctSelected[0] = false;
                                            mBuilder2.dismiss();
                                        }
                                    });
                                    // reset the item background to white
                                    replace.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            correctSelected[0] = false;
                                            caption_listview.getChildAt(i).setBackgroundColor(Color.WHITE);
                                            adapter.notifyDataSetChanged();
                                            // re init correct index
                                            correct[0] = 0;
                                            mBuilder2.dismiss();

                                        }
                                    });
                                    return false;
                                }
                            });
                        }
                    });
                    */

                    // set the current value for the "COUNT" child
                    final int[] current = new int[1];
                    caption_send.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (checkNetwork()) {

                                if (isGroup) {

                                    current[0] = groupCount;

                                    // add to ref of al chats

                                    for (String s : groupMembers) {
                                        // add the chat to the chat ref
                                        System.out.println("this is groupname + group count : " + groupName + " " + groupCount);
                                        System.out.println("This is s : " + s);
                                        System.out.println("this is groupMembers: " + groupMembers.get(0));
                                        ref_chats.child(s).child(groupName + " " + groupCount).setValue("Closed");
                                        // add the chat correct status to the recipients' fb
                                        ref_users.child(user_ids.get(s)).child("Chats").child(groupName).child(groupName + " " + Integer.toString(current[0])).child("Correct").setValue(correctString[0]);

                                        // add the captions
                                        int index = 1;

                                        for (String cap : captions) {
                                            ref_users.child(user_ids.get(s)).child("Chats").child(groupName).child(groupName + " " + Integer.toString(current[0])).child("Captions").child("Cap " + index).setValue(cap);
                                            index++;
                                        }

                                        // add the images to the chat ref for each user
                                        ref_users.child(user_ids.get(s)).child("Chats").child(groupName).child(groupName + " " + Integer.toString(current[0])).child("Image 1").setValue(Base64.encodeToString(image_data, Base64.NO_WRAP));
                                        ref_users.child(user_ids.get(s)).child("Chats").child(groupName).child(groupName + " " + Integer.toString(current[0])).child("Image 2").setValue(Base64.encodeToString(image_data2, Base64.NO_WRAP));
                                        ref_users.child(user_ids.get(s)).child("Chats").child(groupName).child(groupName + " " + Integer.toString(current[0])).child("Image 3").setValue(Base64.encodeToString(image_data3, Base64.NO_WRAP));

                                        // increment the count
                                        ref_users.child(user_ids.get(s)).child("Chats").child(groupName).child("Count").setValue(Integer.toString(groupCount + 1));


                                        // add the time stamp for this chat
                                        //String time = new Date().getMonth() + "/" + new Date().getDay() + " at " + new Date().getHours() + ":" + new Date().getMinutes();
                                        String timeStamp = new SimpleDateFormat("MM/dd").format(Calendar.getInstance().getTime()) +  " at "
                                                + new SimpleDateFormat("hh:mm").format(Calendar.getInstance().getTime());
                                        ref_users.child(user_ids.get(s)).child("Chats").child(groupName).child(groupName + " " + Integer.toString(current[0])).child("Time").setValue(timeStamp);

                                        mBuilder.dismiss();
                                        BuddyUpHome.this.finish();
                                        finish();
                                        finishActivity(5);
                                        finishActivity(6);
                                        finishActivity(7);


                                    }

                                    // add chat correct status to my fb
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(groupName).child(groupName + " " + Integer.toString(current[0])).child("Correct").setValue(correctString[0]);
                                    // add captions
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(groupName).child("Count").setValue(Integer.toString(groupCount + 1));






                                } else {

                                    current[0] = chatCount;

                                    // add to ref of all chats
                                    //ref_chats.child(auth.getCurrentUser().getDisplayName()).child(title_of_page + " " + chatCount).setValue("Closed");
                                    ref_chats.child(title_of_page).child(auth.getCurrentUser().getDisplayName() + " " + chatCount).setValue("Closed");
                                    //ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child("bob").child("Count").setValue(Integer.toString(current[0] + 1));


                                    // set the chat (give it a number and a correct answer in my firebase
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Chat number").setValue(current[0]);
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Correct").setValue(correctString[0]);
                                    // set the chat (give it a num and a correct answer in the recipient's firebase
                                    ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " + Integer.toString(current[0])).child("Chat number").setValue(current[0]);
                                    ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " + Integer.toString(current[0])).child("Correct").setValue(correctString[0]);


                                    // change the value of the count by 1
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child("Count").setValue(Integer.toString(chatCount));
                                    System.out.println("TESTING 2 : " + current[0] + 1);



                                    ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child("Count").setValue(Integer.toString(chatCount));
                                    System.out.println("CHANGED");

                                    // add the image byte arrays in my firebase
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Image 1").setValue(Base64.encodeToString(image_data, Base64.NO_WRAP));
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Image 2").setValue(Base64.encodeToString(image_data2, Base64.NO_WRAP));
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Image 3").setValue(Base64.encodeToString(image_data3, Base64.NO_WRAP));

                                    // add the image byte arrays in recipients firebase

                                    ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " +Integer.toString(current[0])).child("Image 1").setValue(Base64.encodeToString(image_data, Base64.NO_WRAP));
                                    ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " +Integer.toString(current[0])).child("Image 2").setValue(Base64.encodeToString(image_data2, Base64.NO_WRAP));
                                    ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " +Integer.toString(current[0])).child("Image 3").setValue(Base64.encodeToString(image_data3, Base64.NO_WRAP));

                                    int index = 1;
                                    // put captions into firebase chat
                                    for (String s : captions) {
                                        ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Captions").child("Cap " + index).setValue(s);
                                        ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " + Integer.toString(current[0])).child("Captions").child("Cap " + index).setValue(s);
                                        index++;
                                    }

                                    // add the time stamp for this chat
                                    //String time = new Date().getMonth() + "/" + new Date().getDay() + " at " + new Date().getHours() + ":" + new Date().getMinutes();
                                    String timeStamp = new SimpleDateFormat("MM/dd").format(Calendar.getInstance().getTime()) +  " at "
                                            + new SimpleDateFormat("hh:mm").format(Calendar.getInstance().getTime());
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Time").setValue(timeStamp);
                                    ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " + Integer.toString(current[0])).child("Time").setValue(timeStamp);


                                    mBuilder.dismiss();
                                    BuddyUpHome.this.finish();
                                    finish();
                                    finishActivity(5);
                                    finishActivity(6);
                                    finishActivity(7);


                                }

                            } else {
                                mBuilder.dismiss();
                                Snackbar.make(BuddyUpHome.this.findViewById(android.R.id.content), "Unable to send: No network connection", Snackbar.LENGTH_LONG).show();
                            }


                            /*
                            if (checkNetwork()) {

                                current[0] = chatCount;

                                // add to ref of all chats
                                //ref_chats.child(auth.getCurrentUser().getDisplayName()).child(title_of_page + " " + chatCount).setValue("Closed");
                                ref_chats.child(title_of_page).child(auth.getCurrentUser().getDisplayName() + " " + chatCount).setValue("Closed");
                                //ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child("bob").child("Count").setValue(Integer.toString(current[0] + 1));


                                // set the chat (give it a number and a correct answer in my firebase
                                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Chat number").setValue(current[0]);
                                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Correct").setValue(correctString[0]);
                                // set the chat (give it a num and a correct answer in the recipient's firebase
                                ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " + Integer.toString(current[0])).child("Chat number").setValue(current[0]);
                                ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " + Integer.toString(current[0])).child("Correct").setValue(correctString[0]);


                                // change the value of the count by 1
                                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child("Count").setValue(Integer.toString(chatCount));
                                System.out.println("TESTING 2 : " + current[0] + 1);



                                ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child("Count").setValue(Integer.toString(chatCount));
                                System.out.println("CHANGED");

                                // add the image byte arrays in my firebase
                                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Image 1").setValue(Base64.encodeToString(image_data, Base64.NO_WRAP));
                                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Image 2").setValue(Base64.encodeToString(image_data2, Base64.NO_WRAP));
                                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Image 3").setValue(Base64.encodeToString(image_data3, Base64.NO_WRAP));

                                // add the image byte arrays in recipients firebase

                                ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " +Integer.toString(current[0])).child("Image 1").setValue(Base64.encodeToString(image_data, Base64.NO_WRAP));
                                ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " +Integer.toString(current[0])).child("Image 2").setValue(Base64.encodeToString(image_data2, Base64.NO_WRAP));
                                ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " +Integer.toString(current[0])).child("Image 3").setValue(Base64.encodeToString(image_data3, Base64.NO_WRAP));

                                int index = 1;
                                // put captions into firebase chat
                                for (String s : captions) {
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Captions").child("Cap " + index).setValue(s);
                                    ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " + Integer.toString(current[0])).child("Captions").child("Cap " + index).setValue(s);
                                    index++;
                                }

                                // add the time stamp for this chat
                                //String time = new Date().getMonth() + "/" + new Date().getDay() + " at " + new Date().getHours() + ":" + new Date().getMinutes();
                                String timeStamp = new SimpleDateFormat("MM/dd").format(Calendar.getInstance().getTime()) +  " at "
                                        + new SimpleDateFormat("hh:mm").format(Calendar.getInstance().getTime());
                                ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child(title_of_page + " " + Integer.toString(current[0])).child("Time").setValue(timeStamp);
                                ref_users.child(user_ids.get(title_of_page)).child("Chats").child(auth.getCurrentUser().getDisplayName()).child(auth.getCurrentUser().getDisplayName() + " " + Integer.toString(current[0])).child("Time").setValue(timeStamp);


                                mBuilder.dismiss();
                                BuddyUpHome.this.finish();
                                finish();
                                finishActivity(5);
                                finishActivity(6);
                                finishActivity(7);

                            } else {
                                mBuilder.dismiss();
                                Snackbar.make(BuddyUpHome.this.findViewById(android.R.id.content), "Unable to send: No network connection", Snackbar.LENGTH_LONG).show();
                            }
                            */

                        }
                    });

                }

            }
        });

        images.add(mImageView);
        images.add(mImageView1);
        images.add(mImageView2);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            // set prog dialog
            progressDialog.setMessage("Uploading image...");
            progressDialog.show();

            // get the camera image
            Bundle extras = data.getExtras();
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream boas = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, boas);
            byte[] data_boas = boas.toByteArray();

            // set the image int imageview
            if (images.get(counter).getDrawable() == null) {
                //images.get(counter).setImageBitmap((Bitmap) this.getIntent().getExtras().get("Photo"));
                /*
                byte[] bytes = (byte[]) this.getIntent().getExtras().get("Photo");
                System.out.println(bytes);
                Bitmap bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                images.get(counter).setMinimumHeight(metrics.heightPixels);
                images.get(counter).setMinimumWidth(metrics.widthPixels);
                images.get(counter).setImageBitmap(bm);*/

                images.get(counter).setImageBitmap(bitmap);
                counter++;
                if (counter >= 3) {
                    cam_button.setText("Write Captions");
                    // once 3 photos have been taken
                    cam_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // builder for writing captions
                            final AlertDialog mBuilder = new AlertDialog.Builder(BuddyUpHome.this).create();
                            View captionsView = getLayoutInflater().inflate(R.layout.bu_write_caps_alert, null);
                            final EditText caption_edit = (EditText) captionsView.findViewById(R.id.caption_edit);
                            final Button caption_add = (Button) captionsView.findViewById(R.id.caption_add);
                            final ListView caption_listview = (ListView) captionsView.findViewById(R.id.caption_list);
                            final Button caption_send = (Button) captionsView.findViewById(R.id.caption_send);

                            final boolean[] correctSelected = {false};
                            final int[] correct = new int[1];
                            final String[] correctString = new String[1];

                            // disable send button until atleast 2 captions are added
                            caption_send.setEnabled(false);
                            caption_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));

                            mBuilder.setView(captionsView);
                            mBuilder.show();
                            // init an empty array of captions
                            final List<String> captions = new ArrayList<String>();
                            //captions.addAll(saved_captions);
                            // init adapter
                            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(mBuilder.getContext(), android.R.layout.simple_list_item_1, captions);
                            // set the adapter for the list view
                            caption_listview.setAdapter(adapter);
                            // onclick for the + button
                            caption_add.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (!(TextUtils.isEmpty(caption_edit.getText().toString()))) {
                                        captions.add(caption_edit.getText().toString());
                                        //saved_captions = captions;
                                        adapter.notifyDataSetChanged();
                                        caption_edit.getText().clear();
                                        if (captions.size() >= 2 && captions.size() <= 4) {
                                            caption_send.setEnabled(true);
                                            caption_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button));
                                        } else if (captions.size() > 4) {
                                            caption_send.setEnabled(false);
                                            caption_send.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));
                                        }
                                    }
                                }
                            });



                            caption_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                                    if (correctSelected[0] == false){
                                        caption_listview.getChildAt(i).setBackgroundColor(Color.rgb(242, 242, 242));
                                        correctSelected[0] = true;
                                        correct[0] = i;
                                        correctString[0] = caption_listview.getItemAtPosition(i).toString();
                                    }
                                    // open alertDialog to either delete option or replace correct one
                                    caption_listview.getChildAt(i).setOnLongClickListener(new View.OnLongClickListener() {
                                        @Override
                                        public boolean onLongClick(View view) {
                                            final AlertDialog mBuilder2 = new AlertDialog.Builder(mBuilder.getContext()).create();
                                            View v = getLayoutInflater().inflate(R.layout.bu_caption_long_alert, null);
                                            Button trash = (Button) v.findViewById(R.id.bu_trash);
                                            Button replace = (Button) v.findViewById(R.id.bu_replace);

                                            mBuilder2.setView(v);
                                            mBuilder2.show();
                                            trash.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    captions.remove(caption_listview.getItemAtPosition(i).toString());
                                                    adapter.notifyDataSetChanged();
                                                    mBuilder2.dismiss();
                                                }
                                            });
                                            // reset the item background to white
                                            replace.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    correctSelected[0] = false;
                                                    caption_listview.getChildAt(i).setBackgroundColor(Color.WHITE);
                                                    adapter.notifyDataSetChanged();
                                                    // re init correct index
                                                    correct[0] = 0;
                                                    mBuilder2.dismiss();

                                                }
                                            });
                                            return false;
                                        }
                                    });
                                }
                            });

                            caption_send.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    // create a chat inside both the sender and recipient's firebase account

                                    // create chat inside senders database
                                    //ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child("Count").setValue("1");
                                    ref_users.child(auth.getCurrentUser().getUid()).child("Chats").addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                                            System.out.println("This is the new map: " + map);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }

                                    });



                                    //ref_users.child(auth.getCurrentUser().getUid()).child("Chats").child(title_of_page).child("Correct").setValue(correctString[0]);
                                }
                            });

                        }
                    });
                }
            }

            //mImageView.setImageBitmap(bitmap);

            // upload image to firebase
            String filename = auth.getCurrentUser().getDisplayName();
            StorageReference bu_images = mStorage.child("BU Users").child(title_of_page).child(title_of_page + "_" + new Date().getDay() +  "_" + new Date().getHours() + "_" + new Date().getMinutes() + "_ " + new Date().getSeconds());

            // upload image
            UploadTask uploadTask = bu_images.putBytes(data_boas);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                    Toast.makeText(BuddyUpHome.this, "Upload successful", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    // ask user for camera permissions
    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(BuddyUpHome.this, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(BuddyUpHome.this, permission)) {
                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(BuddyUpHome.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(BuddyUpHome.this, new String[]{permission}, requestCode);
            }
        } else {
            //Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }
    // check network
    public boolean checkNetwork() {
        boolean status = false;

        try {
            ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = manager.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            } else {
                netInfo = manager.getNetworkInfo(1);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    status = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return status;
    }








}
