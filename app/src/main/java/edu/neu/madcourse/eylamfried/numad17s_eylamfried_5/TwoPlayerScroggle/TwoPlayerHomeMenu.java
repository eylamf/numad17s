package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.TwoPlayerScroggle;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Comm2.MainAct;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle.ScroggleGameAct;

/**
 * Created by eylamfried on 3/29/17.
 */

public class TwoPlayerHomeMenu extends AppCompatActivity {
    private Button logoutButton;
    private Button chatButton;
    private Button twoPlayerButton;
    private Button friendsListButton;
    private ListView listView;
    private List<String> friends_names = new ArrayList<>();
    private ArrayAdapter<String> arrayAdapter;
    private String recipient_name = "";
    private String recipient_uid;
    private String sender_name = "";
    private String sender_words = "";
    private String sender_letterOrder = "";




    private Button acceptBtn;

    private Button ackButton;
    private Button howToButton;



    //for invite page
    private Button invite;

    //for user id - for inviting
    private Map<String, String> uid_map = new HashMap<String, String>();

    private FirebaseAuth auth;
    private DatabaseReference ref;

    //for position map
    private DatabaseReference refPosMap;
    private String sender_letter_map_0;
    private String sender_letter_map_1;
    private String sender_letter_map_2;
    private String sender_letter_map_3;
    private String sender_letter_map_4;
    private String sender_letter_map_5;
    private String sender_letter_map_6;
    private String sender_letter_map_7;
    private String sender_letter_map_8;


    // TODO TEST BUTTON _ DELETE SOON
    private Button checkChallengesBtn;

    private String challenger_name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_player);
        setTitle("Two Player Word Game");

        auth = FirebaseAuth.getInstance();
        ref = FirebaseDatabase.getInstance().getReference().child("Users");
        refPosMap = ref.child("Position Map");


        logoutButton = (Button) findViewById(R.id.two_player_log_out_btn);
        chatButton = (Button) findViewById(R.id.chat_two_player_btn);
        twoPlayerButton = (Button) findViewById(R.id.two_player_scroggle_btn);
        friendsListButton = (Button) findViewById(R.id.friends_list_btn);

        ackButton = (Button) findViewById(R.id.two_player_ack_button);

        howToButton = (Button) findViewById(R.id.two_player_how_to_button);

        ackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TwoPlayerHomeMenu.this, TwoPlayerAck.class);
                startActivity(intent);
            }
        });

        howToButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TwoPlayerHomeMenu.this, TwoPlayerHowTo.class);
                startActivity(intent);
            }
        });

        //accepting a new game - testing
        //acceptBtn = (Button) findViewById(R.id.accept_btn);

        // TODO TEST BUTTON - WILL DELETE SOON
        checkChallengesBtn = (Button) findViewById(R.id.check_challenges_button);


        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
                finish();
            }
        });

        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TwoPlayerHomeMenu.this, MainAct.class);
                startActivity(intent);
            }
        });

        twoPlayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TwoPlayerHomeMenu.this, ScroggleGameAct.class);
                intent.putExtra("num_players", "1");
                startActivity(intent);
            }
        });

        /*acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TwoPlayerHomeMenu.this, ScroggleGameAct.class);
                intent.putExtra("num_players", "2");
                intent.putExtra("letter_posns", sender_letterOrder);
                intent.putExtra("words", sender_words);

                // letter posn map
                intent.putExtra("sender_letter_map_0", sender_letter_map_0);
                intent.putExtra("sender_letter_map_1", sender_letter_map_1);
                intent.putExtra("sender_letter_map_2", sender_letter_map_2);
                intent.putExtra("sender_letter_map_3", sender_letter_map_3);
                intent.putExtra("sender_letter_map_4", sender_letter_map_4);
                intent.putExtra("sender_letter_map_5", sender_letter_map_5);
                intent.putExtra("sender_letter_map_6", sender_letter_map_6);
                intent.putExtra("sender_letter_map_7", sender_letter_map_7);
                intent.putExtra("sender_letter_map_8", sender_letter_map_8);


                //intent.putExtra("letter_posns_0",);
                //System.out.println("SENDER WORDS: " + sender_words);
                startActivity(intent);

            }
        });
        */

        // uid_map init
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    // uid map
                    Iterator i = dataSnapshot.getChildren().iterator();
                    String user_names;
                    String uid;

                    while (i.hasNext()) {
                        // declare a current DataSnapshot else i.next() will only be able to be used once
                        DataSnapshot curr = ((DataSnapshot)i.next());
                        user_names = curr.child("Username").getValue().toString();
                        uid = curr.getKey();
                        uid_map.put(user_names, uid);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        friendsListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                final AlertDialog mBuilder = new AlertDialog.Builder(TwoPlayerHomeMenu.this).create();
                //reference the alertDialog layout as a view in order to get attributes
                View v = getLayoutInflater().inflate(R.layout.activity_2_player_friends, null);
                final ListView friendsList = (ListView) v.findViewById(R.id.friends_list);

                arrayAdapter = new ArrayAdapter<String>(mBuilder.getContext(), android.R.layout.simple_list_item_1, friends_names);
                friendsList.setAdapter(arrayAdapter);

                ref.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

                        HashMap<String, String> value_map = (HashMap<String, String>) dataSnapshot.getValue();
                        System.out.println("testing...." + value_map.get("Username"));
                        String value = value_map.get("Username"); //+ " - " + value_map.get("Score");

                        if (friends_names.contains(value)) {
                            // do nothing - avoid dups
                        } else {
                            friends_names.add(value);
                        }



                        arrayAdapter.notifyDataSetChanged();
                        System.out.println("Sender words: " + sender_words);
                        System.out.println("Sender letter order: " + sender_letterOrder);
                        System.out.println("TESTING SOMETHING WITH UIDS : " + value_map);
                    }

                    @Override
                    public void onChildChanged(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(com.google.firebase.database.DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(com.google.firebase.database.DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                /*
                arrayAdapter = new ArrayAdapter<String>(mBuilder.getContext(), android.R.layout.simple_list_item_1,
                        friends_names);
                //set the adapter for the list view of friends
                friendsList.setAdapter(arrayAdapter);
                */

                //set the view for the layout
                mBuilder.setView(v);
                AlertDialog dialog = mBuilder;
                dialog.show();


                // check network connectivity
                ConnectivityManager cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo nInfo = cManager.getActiveNetworkInfo();
                if (nInfo != null && nInfo.isConnected()) {
                    Toast.makeText(mBuilder.getContext(), "Network is available", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mBuilder.getContext(), "Network is unavailable", Toast.LENGTH_SHORT).show();
                }


                //handle listView on click event (invite screen)
                final AlertDialog mBuilder2 = new AlertDialog.Builder(TwoPlayerHomeMenu.this).create();
                final View view2 = getLayoutInflater().inflate(R.layout.activity_invite_player, null);
                invite = (Button) view2.findViewById(R.id.invite_friend_btn);
                final TextView challengerName = (TextView) view2.findViewById(R.id.challenge_recipient_text);
                final TextView challengerScore = (TextView) view2.findViewById(R.id.challenge_recipient_score_text);


                friendsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        //Intent intent = new Intent(mBuilder.getContext(), mBuilder2.getContext().getClass());
                        //intent.putExtra("room_name", ((TextView)view).getText().toString() );
                        //intent.putExtra("user_name", "test");
                        //startActivity(intent);



                        recipient_name = friendsList.getItemAtPosition(position).toString();
                        challengerName.setText(recipient_name);

                        ref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Map<String, HashMap<String, String>> map = (Map<String, HashMap<String, String>>) dataSnapshot.getValue();
                                challengerScore.setText("High score: " + map.get(uid_map.get(recipient_name)).get("Score"));
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                        mBuilder2.setView(view2);
                        AlertDialog dialog = mBuilder2;
                        dialog.show();

                        System.out.println("This is the recipient : " + recipient_name);
                        // on click for invite button
                        invite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // must get the board info from the user who sends the invite
                                //DatabaseReference test = ref.child(uid_map.get(recipient_name));
                                //System.out.println("WHAT IS THIS: " + test);

                                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                                        //dataSnapshot.getRef().child(uid_map.get(sender_name)).child("Words");


                                        // create a uid hash map

                                            /*
                                            Iterator i = dataSnapshot.getChildren().iterator();
                                            while (i.hasNext()) {
                                                String uid = ((DataSnapshot) i.next()).getKey();
                                                System.out.println("This is the uid test : " + uid);

                                            }
                                            */





                                        try {

                                            /*
                                            // uid map
                                            Iterator i = dataSnapshot.getChildren().iterator();
                                            String user_names;
                                            String uid;

                                            while (i.hasNext()) {
                                                // declare a current DataSnapshot else i.next() will only be able to be used once
                                                DataSnapshot curr = ((DataSnapshot)i.next());
                                                user_names = curr.child("Username").getValue().toString();
                                                uid = curr.getKey();
                                                uid_map.put(user_names, uid);
                                            }
                                            System.out.println("This is uid_map: " + uid_map);
                                            */

                                            // words, letter posn, sender name



                                            Map<String, HashMap<String, String>> map = (Map<String, HashMap<String, String>>)dataSnapshot.getValue();
                                            System.out.println("Testing something... : " + uid_map);


                                            sender_name = map.get(auth.getCurrentUser().getUid()).get("Username");
                                            sender_words = map.get(auth.getCurrentUser().getUid()).get("Words");
                                            sender_letterOrder = map.get(auth.getCurrentUser().getUid()).get("Letter Order");
                                            // get the letter order map in form of ArrayList<Integer>
                                            Map<String, HashMap<String, ArrayList<Object>>> pos_map =
                                            (Map<String, HashMap<String, ArrayList<Object>>>) dataSnapshot.getValue();

                                            sender_letter_map_0 = pos_map.get(auth.getCurrentUser().getUid()).get("Position Map").get(0).toString();
                                            sender_letter_map_1 = pos_map.get(auth.getCurrentUser().getUid()).get("Position Map").get(1).toString();
                                            sender_letter_map_2 = pos_map.get(auth.getCurrentUser().getUid()).get("Position Map").get(2).toString();
                                            sender_letter_map_3 = pos_map.get(auth.getCurrentUser().getUid()).get("Position Map").get(3).toString();
                                            sender_letter_map_4 = pos_map.get(auth.getCurrentUser().getUid()).get("Position Map").get(4).toString();
                                            sender_letter_map_5 = pos_map.get(auth.getCurrentUser().getUid()).get("Position Map").get(5).toString();
                                            sender_letter_map_6 = pos_map.get(auth.getCurrentUser().getUid()).get("Position Map").get(6).toString();
                                            sender_letter_map_7 = pos_map.get(auth.getCurrentUser().getUid()).get("Position Map").get(7).toString();
                                            sender_letter_map_8 = pos_map.get(auth.getCurrentUser().getUid()).get("Position Map").get(8).toString();

                                            System.out.println("HUGE RISK TESt: " + sender_letter_map_0);//pos_map.get(auth.getCurrentUser().getUid()).get("Position Map"));//pos_map.get(auth.getCurrentUser().getUid()).get("Position Map").get(0).getClass());






                                            // set value for invited and invited by
                                            ref.child(auth.getCurrentUser().getUid()).child("Invited").setValue(recipient_name);
                                            ref.child(uid_map.get(recipient_name)).child("Invited by").setValue(sender_name);
                                            ref.child(uid_map.get(recipient_name)).child("Challenged").setValue("true");




                                            System.out.println("THIS IS THE sender : " + map.get(auth.getCurrentUser().getUid()).get("Words"));
                                            System.out.println("THIS IS SNAPSHOT " + dataSnapshot.child("Words").getValue());
                                        } catch (Exception e) {
                                            Toast.makeText(TwoPlayerHomeMenu.this, "Must play a game first", Toast.LENGTH_SHORT).show();
                                            e.printStackTrace();
                                        }


                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });


                            }
                        });

                    }
                });

            }
        });

        final AlertDialog challBuilder = new AlertDialog.Builder(TwoPlayerHomeMenu.this).create();
        //reference the alertDialog layout as a view in order to get attributes
        View challView = getLayoutInflater().inflate(R.layout.activity_accept_challenge, null);
        Button accept_btn = (Button) challView.findViewById(R.id.accept_game_button);
        Button decline_btn = (Button) challView.findViewById(R.id.decline_game_button);
        final TextView challenger_text = (TextView) challView.findViewById(R.id.text_challenger);
        // set view for chall alertDialog builder
        challBuilder.setView(challView);

        // onclick for accept challenge button
        accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {
                    Intent intent = new Intent(TwoPlayerHomeMenu.this, ScroggleGameAct.class);
                    intent.putExtra("num_players", "2");
                    intent.putExtra("letter_posns", sender_letterOrder);
                    intent.putExtra("words", sender_words);

                    // letter posn map
                    intent.putExtra("sender_letter_map_0", sender_letter_map_0);
                    intent.putExtra("sender_letter_map_1", sender_letter_map_1);
                    intent.putExtra("sender_letter_map_2", sender_letter_map_2);
                    intent.putExtra("sender_letter_map_3", sender_letter_map_3);
                    intent.putExtra("sender_letter_map_4", sender_letter_map_4);
                    intent.putExtra("sender_letter_map_5", sender_letter_map_5);
                    intent.putExtra("sender_letter_map_6", sender_letter_map_6);
                    intent.putExtra("sender_letter_map_7", sender_letter_map_7);
                    intent.putExtra("sender_letter_map_8", sender_letter_map_8);


                    //intent.putExtra("letter_posns_0",);
                    //System.out.println("SENDER WORDS: " + sender_words);
                    startActivity(intent);

                    // re init 'challenged' value to false
                    ref.child(auth.getCurrentUser().getUid()).child("Challenged").setValue("false");
                    // re init invited by value and invite value
                    //ref.child(auth.getCurrentUser().getUid()).child("Invited by").setValue("");


                    // dismiss the alertDialog
                    challBuilder.dismiss();


                } catch (Exception e) {
                    e.printStackTrace();
                }







            }
        });


        // onclick for decline challenge button
        decline_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                challBuilder.dismiss();
            }
        });

        // add onclick event for challenged button
        checkChallengesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // inner layer - current user
                ref.child(auth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        try {

                            Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                            System.out.println("Challenge button: " + map);

                            if (map.get("Challenged").equals("true")) {
                                challenger_text.setText("Challenger: " + map.get("Invited by"));
                                challBuilder.show();
                            } else {
                                //Toast.makeText(TwoPlayerHomeMenu.this, "No challenges to view", Toast.LENGTH_SHORT).show();
                            }

                            challenger_name = map.get("Invited by");


                            // set up board

                            Map<String, HashMap<String, String>> map_info = (Map<String, HashMap<String, String>>)dataSnapshot.getValue();
                            System.out.println("This is map info: " + map_info);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        try {

                            Map<String, HashMap<String, String>> info_map = (Map<String, HashMap<String, String>>) dataSnapshot.getValue();
                            sender_words = info_map.get(uid_map.get(challenger_name)).get("Words");

                            Map<String, HashMap<String, ArrayList<Object>>> pos_map =
                                    (Map<String, HashMap<String, ArrayList<Object>>>) dataSnapshot.getValue();


                            sender_letter_map_0 = pos_map.get(uid_map.get(challenger_name)).get("Position Map").get(0).toString();
                            sender_letter_map_1 = pos_map.get(uid_map.get(challenger_name)).get("Position Map").get(1).toString();
                            sender_letter_map_2 = pos_map.get(uid_map.get(challenger_name)).get("Position Map").get(2).toString();
                            sender_letter_map_3 = pos_map.get(uid_map.get(challenger_name)).get("Position Map").get(3).toString();
                            sender_letter_map_4 = pos_map.get(uid_map.get(challenger_name)).get("Position Map").get(4).toString();
                            sender_letter_map_5 = pos_map.get(uid_map.get(challenger_name)).get("Position Map").get(5).toString();
                            sender_letter_map_6 = pos_map.get(uid_map.get(challenger_name)).get("Position Map").get(6).toString();
                            sender_letter_map_7 = pos_map.get(uid_map.get(challenger_name)).get("Position Map").get(7).toString();
                            sender_letter_map_8 = pos_map.get(uid_map.get(challenger_name)).get("Position Map").get(8).toString();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



            }
        });





        /*
        // adds usernames to "friends" alertDialog
        ref.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                try {
                    Iterator i = dataSnapshot.getChildren().iterator();
                    Map<String, HashMap<String, String>> map = (Map<String, HashMap<String, String>>)dataSnapshot.getValue();

                    String uid;
                    while(i.hasNext()) {

                        HashMap<String, String> temp = (HashMap<String, String>)((com.google.firebase.database.DataSnapshot)i.next()).getValue();
                        String name = temp.get("Username");
                        uid = ((com.google.firebase.database.DataSnapshot) i.next()).getKey();

                        System.out.println("THIS IS THE UID : " + uid);
                        // avoid creating duplicates
                        if (friends_names.contains(name)) {
                            // do nothing - don't want duplicates
                        } else {
                            friends_names.add(name);
                            uid_map.put(name, uid);
                        }

                        System.out.println("THE MAP: " + temp.get("Username"));

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        */






    }
}
