package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Comm.realtimedatabase;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by eylamfried on 3/14/17.
 */


import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Comm.CommunicationActivity;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

//TODO https://developer.android.com/reference/android/support/design/widget/Snackbar.html

public class RealtimeDatabaseActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    private CoordinatorLayout mCoor;
    String getTokenInst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realtime_db);

        try {
            mCoor = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = connManager.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                new CommunicationActivity().changeTextStatus(true);
            } else {
                Snackbar snackbar = Snackbar.make(mCoor,
                        "Unable to get internet connection", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(RealtimeDatabaseActivity.this, CommunicationActivity.class);
                                RealtimeDatabaseActivity.this.startActivity(intent);
                            }
                        });
                snackbar.setActionTextColor(Color.RED);
                View view = snackbar.getView();
                TextView textview = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                textview.setTextColor(Color.YELLOW);
                snackbar.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getTokenInst = FirebaseInstanceId.getInstance().getToken();
        /*
        Button enterWordBtn = (Button) findViewById(R.id.enter_word_button);
        enterWordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RealtimeDatabaseActivity.this, AddWordDB.class);
                startActivity(intent);
            }
        });

        Button viewScoreBtn = (Button) findViewById(R.id.view_score_button);
        viewScoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RealtimeDatabaseActivity.this, ViewScoreActivity.class);
                startActivity(intent);
            }
        });
        */

    }
}
