package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 4/13/17.
 */

public class Settings extends AppCompatActivity {

    // init firebase authentication
    private FirebaseAuth auth;

    // firebase database
    private FirebaseDatabase mDatabase;

    // ref for users in firebase database
    private DatabaseReference users_ref;
    private DatabaseReference users_pass;

    // init buttons for settings view
    private Button account_button;
    private Button notif_button;
    private Switch camera_switch;
    private Button logout_button;

    // for changing the password
    private String oldPass;
    private String newPass;

    private ProgressDialog progressDialog;

    private View v;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Settings");
        setContentView(R.layout.activity_bu_settings);



        // init firebase
        auth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        users_ref = mDatabase.getReference().child("BU Users").child(auth.getCurrentUser().getUid());
        users_pass = users_ref.child("Password");


        // connect buttons to ids
        account_button = (Button) findViewById(R.id.bu_settings_account);
        notif_button = (Button) findViewById(R.id.bu_settings_notif);
        camera_switch = (Switch) findViewById(R.id.bu_settings_camera);
        logout_button = (Button) findViewById(R.id.bu_settings_logout);



        if (ContextCompat.checkSelfPermission(Settings.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            camera_switch.setChecked(true);
        } else {
            camera_switch.setChecked(false);
        }

        account_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // init v (view) for future use (Snackbars)
                v = view;
                final AlertDialog mBuilder = new AlertDialog.Builder(Settings.this).create();
                View accountView = getLayoutInflater().inflate(R.layout.bu_account_alert, null);
                final Button saved_changes_button = (Button) accountView.findViewById(R.id.account_submit_button);
                final EditText account_email = (EditText) accountView.findViewById(R.id.account_email);
                final EditText account_old = (EditText) accountView.findViewById(R.id.account_old_pass);
                final EditText account_new = (EditText) accountView.findViewById(R.id.account_new_pass);
                // set view for mBuilder and show builder
                mBuilder.setView(accountView);
                mBuilder.show();

                // set progress dialog
                progressDialog = new ProgressDialog(mBuilder.getContext());


                Toast.makeText(mBuilder.getContext(), "Please verify your email", Toast.LENGTH_SHORT).show();


                account_old.setEnabled(false);
                account_new.setEnabled(false);
                saved_changes_button.setEnabled(false);
                //saved_changes_button.setBackgroundColor(Color.rgb(65, 13, 122));
                saved_changes_button.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));


                account_email.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (auth.getCurrentUser().getEmail().equalsIgnoreCase(account_email.getText().toString())) {
                            account_old.setEnabled(true);
                        } else {
                            //Toast.makeText(mBuilder.getContext(), "Email not found", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                account_old.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (account_old.getText().toString().equalsIgnoreCase(oldPass)) {
                            account_new.setEnabled(true);
                        }
                    }
                });

                account_new.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        if (TextUtils.isEmpty(account_new.getText().toString()) || account_new.getText().toString().length() < 6 ||
                                account_new.getText().toString().equalsIgnoreCase(oldPass)) {
                            saved_changes_button.setEnabled(false);
                            //saved_changes_button.setBackgroundColor(Color.rgb(65, 13, 122));
                            saved_changes_button.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button_dark));
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if (!account_new.getText().toString().equalsIgnoreCase(oldPass) && account_new.getText().toString().length() >= 6) {
                            saved_changes_button.setEnabled(true);
                            //saved_changes_button.setBackgroundColor(Color.rgb(117, 26, 214));
                            saved_changes_button.setBackgroundDrawable(getResources().getDrawable(R.drawable.bu_button));
                            newPass = account_new.getText().toString();
                        }
                        if (account_new.getText().toString().equalsIgnoreCase(oldPass)) {

                            Toast.makeText(mBuilder.getContext(), "Please enter a new password", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


                users_ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Map<String, String> map = (HashMap<String, String>) dataSnapshot.getValue();
                        String user_pass = map.get("Password").toString();
                        oldPass = user_pass;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                saved_changes_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        progressDialog.setMessage("Updating account");
                        progressDialog.show();
                        users_pass.setValue(account_new.getText().toString());
                        auth.getCurrentUser().updatePassword(account_new.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                mBuilder.dismiss();
                                progressDialog.dismiss();
                                //Toast.makeText(Settings.this, "Account updated", Toast.LENGTH_SHORT).show();
                                Snackbar.make(v, "Account Updated", Snackbar.LENGTH_SHORT).setAction("Action", null).show();


                            }
                        });

                    }
                });


            }
        });

        logout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                auth.signOut();
                finish();
                finishActivity(1);
                finishActivity(0);
                startActivity(new Intent(Settings.this, LoginPage.class));
            }
        });

        notif_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // do nothing for now
            }
        });




    }


}
