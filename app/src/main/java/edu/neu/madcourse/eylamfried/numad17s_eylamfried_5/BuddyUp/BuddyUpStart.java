package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.BuddyUp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.MainActivity_main;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 4/23/17.
 */

public class BuddyUpStart extends AppCompatActivity {

    private TextView app_desc;
    private Button startBU;
    private Button ack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bu_buddy_up_start_act);
        setTitle("BuddyUp");

        app_desc = (TextView) findViewById(R.id.app_description);
        startBU = (Button) findViewById(R.id.start_bu);
        ack = (Button) findViewById(R.id.bu_ack_button);


        app_desc.setText("BuddyUp is a social sharing application. Simply take 3 photos, input options for what " +
                "activity you were doing, and send them to a friend. The recipient then has to choose one of " +
                "the options based off the given images. If he or she is correct, your score goes up." +
                "\n" + "\n" +
                "Want to share with more than one friend? No problem. Send messages to several friends at " +

                "the same time by creating a group. Each member of the group then guesses individually, and " +

                "your score with each member increments accordingly.");

        startBU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BuddyUpStart.this, LoginPage.class);
                startActivity(intent);
                finish();
            }
        });

        ack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog mBuilder = new AlertDialog.Builder(BuddyUpStart.this).create();
                View v = getLayoutInflater().inflate(R.layout.bu_ack, null);
                TextView text = (TextView) v.findViewById(R.id.bu_ack_text);

                text.setText("YouTube channels: \n" + "thenewboston and TVAC Studio \n \n" + "We would also like to thank the TA's for " +
                        "helping us with many android issues. We also read the documents on the Android Camera API here : \n" +  "https://developer.android.com/guide/topics/media/camera.html" + "\n \n" + "Links:\n" + "https://www.youtube.com/user/akshayejh/videos \n" +
                        "https://www.youtube.com/user/thenewboston"
                );

                mBuilder.setView(v);
                mBuilder.show();

            }
        });



    }
}
