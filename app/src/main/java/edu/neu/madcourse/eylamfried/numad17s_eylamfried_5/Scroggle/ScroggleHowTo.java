package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 2/17/17.
 */

public class ScroggleHowTo extends AppCompatActivity {

    TextView scroggleHow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scroggle_how_to_play);

        scroggleHow = (TextView) findViewById(R.id.scroggle_how_to);


        String howToPlay =
                "1) Click letters in order to form a word. You may only select letters within the same grid.\n" +
                        "2) Hit the 'submit' button to check whether your selected tiles form a word.\n" +
                        "3) If your selection does NOT form a word, re-click the tiles to 'deselect' them.\n" +
                        "3) Once the timer runs out, Phase 2 will begin.\n" +
                        "4) In Phase 2, you may only select letters within the blue tiles to form a word.\n" +
                        "5) During Phase 2, you may select a blue tile anywhere.\n" +
                        "6) Once selected, the tile will then turn green, meaning it only has one more use available.\n" +
                        "7) If a green tile is selected again, it turns red and is not longer valid for reuse";

        scroggleHow.setText(howToPlay);
    }

}
