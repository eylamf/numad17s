package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5;

import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

public class AboutActivity extends AppCompatActivity {

    TextView phone_id_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        phone_id_value = (TextView) findViewById(R.id.phone_id);
        //get the phone ID
        phone_id_value.setText("Phone ID: " + Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID));

    }
}
