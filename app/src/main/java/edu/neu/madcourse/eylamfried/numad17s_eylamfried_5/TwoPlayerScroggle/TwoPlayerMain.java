package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.TwoPlayerScroggle;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Comm2.MainAct;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Communication.CommMain;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Communication.CommMainMenu;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Communication.User;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 3/29/17.
 */

public class TwoPlayerMain extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;

    private String emailStr;
    private String passwordStr;

    private FirebaseAuth.AuthStateListener authStateListener;

    private DatabaseReference root;
    private DatabaseReference root2;

    private static final String TAG = CommMain.class.getSimpleName();

    private FirebaseUser firebaseUser;
    private User user;


    // for after login page - 2 player main menu
    private Button logoutButton;
    private Button chatButton;
    private Button twoPlayerButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_player_sign_in);
        setTitle("Two Player Word Game");




        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);



        //TODO signs user out each time - can get rid of this
        //firebaseAuth.signOut();

        // Users root in db
        root = FirebaseDatabase.getInstance().getReference().child("Users");
        //root2 = root.child("WORDS");
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    startActivity(new Intent(TwoPlayerMain.this, TwoPlayerHomeMenu.class));

                    System.out.println(firebaseAuth.getCurrentUser().getEmail().toString());
                }
            }
        };

        firebaseUser = firebaseAuth.getCurrentUser();

        // for after log in page - main menu



        //f
        //login material
        Button mLogin = (Button) findViewById(R.id.log_in_b_2);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog mBuilder = new AlertDialog.Builder(TwoPlayerMain.this).create();
                //reference the alertDialog layout as a view in order to get attributes
                View v = getLayoutInflater().inflate(R.layout.login_view, null);
                final EditText mEmail = (EditText) v.findViewById(R.id.login_email);
                final EditText mPassword = (EditText) v.findViewById(R.id.login_pass);
                Button mLoginBtn = (Button) v.findViewById(R.id.login_loginButton);


                mLoginBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //System.out.println(user.getEmail().toString());
                        emailStr = mEmail.getText().toString();
                        passwordStr = mPassword.getText().toString();
                        //user = new User(firebaseUser.getDisplayName(), 0);




                        if (TextUtils.isEmpty(emailStr) || TextUtils.isEmpty(passwordStr)) {
                            Toast.makeText(TwoPlayerMain.this, "Please fill out each field", Toast.LENGTH_SHORT).show();
                        } else {

                            firebaseAuth.signInWithEmailAndPassword(emailStr, passwordStr).addOnCompleteListener(
                                    new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {

                                            if(!task.isSuccessful()) {
                                                Toast.makeText(TwoPlayerMain.this, "Sign in error, try again", Toast.LENGTH_SHORT).show();

                                            } else {
                                                Toast.makeText(TwoPlayerMain.this, "Login successful", Toast.LENGTH_SHORT).show();
                                                mBuilder.dismiss();
                                                //user.setStatus("online");
                                            }

                                        }
                                    }
                            );

                        }
                    }
                });

                //set the view for the layout
                mBuilder.setView(v);
                AlertDialog dialog = mBuilder;
                dialog.show();
            }
        });


        Button mSignup = (Button) findViewById(R.id.sign_up_b_2);
        mSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog mBuilder2 = new AlertDialog.Builder(TwoPlayerMain.this).create();
                //reference the alertDialog layout as a view in order to get attributes
                View mView = getLayoutInflater().inflate(R.layout.sign_up_view, null);
                final EditText mEmail = (EditText) mView.findViewById(R.id.signup_email);
                final EditText mUsername = (EditText) mView.findViewById(R.id.signup_username);
                final EditText mPassword = (EditText) mView.findViewById(R.id.signup_pass);
                Button mSignupBtn = (Button) mView.findViewById(R.id.signup_signupButton);
                mSignupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(!(mEmail.getText().toString().isEmpty()) && !(mUsername.getText().toString().isEmpty()) &&
                                !(mPassword.getText().toString().isEmpty())) {


                            firebaseAuth.createUserWithEmailAndPassword(mEmail.getText().toString(), mPassword.getText().toString()).addOnCompleteListener(
                                    new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {

                                            if(task.isSuccessful()) {
                                                //successful
                                                Toast.makeText(TwoPlayerMain.this, "Sign up successful", Toast.LENGTH_SHORT).show();
                                                mBuilder2.dismiss();


                                                User new_user = new User(mUsername.getText().toString(), 0);
                                                new_user.setStatus("online");


                                                //DatabaseReference user_name = mDatabase.child("name");
                                                //root.push().setValue(new_user);
                                                root2 = root.child(firebaseAuth.getCurrentUser().getUid());

                                                root2.child("Username").setValue(mUsername.getText().toString());
                                                root2.child("Score").setValue("0");
                                                root2.child("Status").setValue("Offline");
                                                root2.child("Words").setValue(new_user.getWords().toString());
                                                root2.child("Letter Order").setValue(new ArrayList<String>(new ArrayList<String>()).toString());
                                                root2.child("Invited by").setValue("");
                                                root2.child("Invited").setValue("");
                                                root2.child("Position Map").setValue("");
                                                root2.child("Challenged").setValue("false");

                                                //root2.setValue("helloWorld");
                                                //update the User's display name
                                                FirebaseUser user = firebaseAuth.getCurrentUser();

                                                UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                                                        .setDisplayName(mUsername.getText().toString()).build();


                                                user.updateProfile(profileUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Log.d(TAG, "User profile updated");
                                                        }
                                                    }
                                                });




                                            } else {
                                                Toast.makeText(TwoPlayerMain.this, "Could not register. Please try again", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }
                            );

                        } else {
                            Toast.makeText(TwoPlayerMain.this, "Please fill out each field", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                mBuilder2.setView(mView);
                AlertDialog dialog2 = mBuilder2;
                dialog2.show();
            }
        });



    }


    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

}
