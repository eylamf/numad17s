package edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Scroggle;

import android.app.Activity;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.os.Bundle;

import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.Dictionary.DictionaryApp;
import edu.neu.madcourse.eylamfried.numad17s_eylamfried_5.R;

/**
 * Created by eylamfried on 2/17/17.
 */

public class ScroggleMainActivity extends Activity {

    MediaPlayer mediaPlayer;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle saveInstanceState) {

        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_scroggle_main);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayer = MediaPlayer.create(this, R.raw.numad_bg_music);
        mediaPlayer.setVolume(0.5f, 0.5f);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.stop();
        mediaPlayer.reset();
        mediaPlayer.release();
    }
}
